---
title: 'À propos'
date: 2022-04-24T00:00:00+00:00
url: /a-propos/
---
Rémi Verchère, Consultant Cloud Native Infrastructure chez Accenture depuis quelques temps.

J'écris ici quelques notes techniques autour de mes sujets de prédilection :
- Les infrastructures à base de conteneurs
- L'intégration & déploiement continus
- L'observabilité
- Eventuellement d'autres sujets divers & variés

L'idée est de partager modestement mes connaissances avec la communauté tech fr. Les articles seront ici rédigés exclusivement en français.

Vous trouverez la liste des mes supports de présentations ici: [présentations](https://www.vrchr.fr/presentations/)

Enfin, rendez-vous sur les divers réseaux sociaux si vous souhaitez échanger :
* [LinkedIn](https://www.linkedin.com/in/rverchere/)
* [Bluesky](https://r.verchere.fr)
