---
title: "Notes Kubernetes Community Days France 2023"
date: 2023-03-07T23:59:59Z
thumbnail: /2023/03/2023-03-07-kcdfr.png
categories:
  - Conférence
tags:
  - IT
  - Développement
  - Kubernetes
description: Retour sur la conférence Kubernetes Community Days France 2023.
toc: true
---

## Kubernetes Community Days France 2023

Début Mars 2023 s'est tenu le premier KCD France, événement communautaire autour de Kubernetes et l'environnement Cloud Natif.

Etant très friand de ces sujets ces derniers temps, il était hors de question que je loupe cela !

De plus, j'ai eu le privilègre d'être sélectionné pour 2 sujets :
- Comment bien foirer sa certification CK{A,S}
- Table Ronde Communautaire - Comment faciliter les contributions open source et mieux les évangéliser

Malgré un contexte de société difficile en cette période (réforme des retraites), les bénévoles organisateurs ont tout fait pour maintenir l'événement, et donc en tant que speaker c'était tout à fait normal de supporter leur choix, et aller dans leur sens. Alors à moi Paris pour encore quelques jours !

## Avant-goût (Meetup Paris Open Source Data Infrastructure)

Initialement, j'avais prévu un aller-retour dans la journée du mardi, mes 2 sessions étant prévues l'après-midi. Cependant, ce jour étant annoncé comme journée de grève nationale, j'ai étendu mon déplacement, arrivant sur Paris le lundi soir pour repartir le mercredi midi. N'ayant alors rien de prévu la veille de l'événement, et par le hasard du calendrier un meetup organisé par [Sébastien Blanc](https://twitter.com/sebi2706) d'Aiven sur l'infra, la data & l'Open Source était prévu dans les locaux d'OVHcloud !

J'ai donc présenté mon rex sur Velero (outil de sauvegarde d'applications sous Kubernetes), à côté d'autres speakers ([Aurélie Vache](https://twitter.com/aurelievache) & [Horacio Gonzàlez](https://twitter.com/LostInBrittany) d'OVHcloud, [Arthur Busser](https://twitter.com/ArthurBusser) de chez Pigment).

Soirée en toute intimité, donnant un avant-goût de LA journée KCD !

## Le Jour J : la conférence, ambiance, environnement, etc.

Après pas mal de déboires, le KCD France a eu ENFIN lieu en physique, avec une équipe organisatrice remontée à bloc ! Je ne vais pas faire de redite, mais le parcours pour organiser cet événement a été semé d'embuches, et les bénévoles s'en sont finalement très bien sortis. Bravo à eux !

Concernant le lieu, la conférence se déroulait au Centre Pompidou, en plein centre de Paris. L'espace était grand, 3 salles "Bleu", "Blanc" et "Rouge" pour les sessions, un espace stands sponsors & partenaires, et un espace "Communautaire" sur une mezzannine, qui aurait mérité un peu plus de visibilité. On avait même droit à une retransmission sur grand écran dans le hall pour les malheureux qui n'avaient plus de place.

Après un peu d'attente pour entrer (la gestion des badges est à noter dans les axes d'amélioration), tout le reste de la journée s'est déroulé sans accrocs !

Toujours content de croiser les personnes avec qui j'échange sur le net, des anciens & actuels collègues, et même des amis de Lycée. Mention spéciale à [Aurélien Violet](https://www.linkedin.com/in/aur%C3%A9lien-violet-bb97445/) avec qui j'ai pas mal échangé avant l'événement et que j'ai eu l'occasion de croiser en vrai.

![Un Rémi content devant le KCD](/2023/03/2023-03-07-kcdfr-rve.jpg)

Place maintenant aux sessions auxquelles j'ai pu assister !

## Keynote d'ouverture

Arrivé avec un peu de retard sur la keynote (ça commence bien), j'ai pris au vol les échanges entre [Solomon Hykes](https://twitter.com/solomonstre) et [Jérôme Petazzoni](https://twitter.com/jpetazzo) sur Docker, et le nouveau projet Dagger.io.

La vidéo de la keynote est disponible [ici](https://youtu.be/OKIehz7p4ug).

### Echanges avec Solomon Hykes & Dagger.io

Ce projet semble très prometteur, car tout l'éco-système de la CI/CD se cherche encore (parlez-en à l'équipe [R2Devops](https://twitter.com/r2devops_io) !), et Dagger essaie de rationnaliser tout cela (fini les commits "update CI/CD" entre autre). Dagger vient notamment de "DAG" pour "Directed Acyclic Graph".

![Dagger](/2023/03/2023-03-07-kcdfr-dagger.jpg)

Nous avions fait un workshop en interne sur le sujet, mais l'outil évolue très vite qu'il faudra retester.

### Red Hat Openshift

Red Hat est une entreprise que j'apprécie beaucoup, pour toutes les contributions qu'elle peut faire (*ndlr: je suis RHCE, j'espère bientôt RHCA*).

Elle nous présente alors OpenShift, avec une définition super simple : Kubernetes avec beaucoup d'amour ;)

![Openshift](/2023/03/2023-03-07-kcdfr-openshift.jpg)


### REX de l'Education Nationale

L'Education Nationale nous a fait un retour de l'utilisation de Kubernetes, notament sur la gestion des copies du Baccalauréat avec le projet SANTORIN.

L'équipe n'est pas grand,e mais les challenges le sont ! Et avec beaucoup d'outils (et de volonté), ils proposent une belle usine logicielle pour faire tourner tout cela, avec un socle Kubernetes bien sûr.

### Talks Sponsors

Les sponsors Platinum ont présentés leurs produits, mais c'est Scaleway qui saura se faire remarquer avec quelques minutes de rafraichissement sur la prononciation de `kubectl`.

## La plus-value d'un portail développeur chez Back Market

Place maintenant aux sessions techniques / rex ! On commence par [Sami Farhat](https://www.linkedin.com/in/farhatsami) de Black Market qui nous explique comment ils en sont arrivé à mettre en place un "Developer Portal" avec l'outil Backstage.

L'idée ici est d'avoir un portail 'self-service' pour faciliter la vie des devs, qu'ils soient le plus autonomes pour travailler et gérer leurs environnements à la demande.

Si l'idée est top (on va en entendre parler avec les platform engineers), il faut cependant considérer la mise en place d'un tel outil comme un produit à part entière, qu'il soit fiable pour que l'adoption soit complète, et que ce ne soit pas un nième outil d'automatisation ni fait ni à faire.

![backstage](/2023/03/2023-03-07-kcdfr-backstage.jpg)

La vidéo est disponible [ici](https://youtu.be/2XghfHsbRtw).

### Commentaires

> La notion d'IDP est très intéressante, à suivre de prêt. J'aurais bien aimé une "démo" de leur plateforme pour mieux me rendre compte du travail fourni.
> Etrange aussi, la conf été en Anglais, alors que le speaker maitrisait bien le Français pendant les questions/réponses.

## Sécurisez votre software supply chain avec SLSA, Sigstore et Kyverno

Moi qui pensait assister à une conf sur une extension des SLAs, j'ai été surpris, mais pas dépaysé car la sécurité reste un enjeu majeur dans les environnements Cloud Native.

[Mohamed Abdennebi](https://www.linkedin.com/in/abdennebi/) de Sfeir nous présente alors SLSA, qui est la pour répondre au besoin d'assurer l'intégrité de la supply chain. Plusieurs niveaux d'exigence existe, avec diverses solutions à mettre en oeuvre, notamment Sigstore & Kyverno.

Les différents outils de Sigstore nous ont été présenté (Cosign le plus connu, mais aussi Fulcio et Rekor), avec une démo de chaque étape de la sécurisation de la Supply Chain.

Comme à chaque fois quand on parle sécurité, beaucoup d'actions à faire pour s'assurer de la conformité des applications de bout en bout, c'est un sujet où l'on ne risque pas de s'ennuyer !

![SLSA](/2023/03/2023-03-07-kcdfr-slsa.jpg)

La vidéo est disponible [ici](https://youtu.be/cWpC96J05gI).

### Commentaires

> Présentation claire de Mohamed, sur l'ensemble des composants à mettre en place pour être SLSA compliant.

## Shift Happen : Sécurité de vos environnements Cloud Native

Un lightning talk de [Stéphane Montri](https://www.linkedin.com/in/smontri/) de chez Palo Alto sur la sécurité de bout en bout d'un projet.. ça me rappelle le talk juste au dessus tiens ! ;)

On ne le dit jamais assez, le niveau de sécurité d'un projet est conditionné par le maillon le plus faible de la chaîne... Et cette chaîne est longue :
- IDE
- Gestionnaire de code source
- CI/CD
- Registre d'image
- Admission Controller
- Applications tierces
- etc.

Bref, encore et toujours du boulot pour la sécu !

![SLSA](/2023/03/2023-03-07-kcdfr-prisma.jpg)

La vidéo est disponible [ici](https://youtu.be/x39zn0dNf88).

## Comment bien foirer sa certification CK{A,S}

A moi de jouer ! Suite à 2 passages de certifications foireux, j'ai décidé l'an dernier d'en faire un article de blog, qui a été transformé en mini-conf au Camping des Speakers 2021, pour au final être présenté ici-même ! Je n'en revenais pas !

Pendant 10 minutes, j'ai partagé quelques non-astuces pour louper sa certif, histoire de dédramatiser un peu la situation, montrant aux plus jeunes ou moins expérimentés que tout le monde peut se planter.

Je me suis REGALE à faire cette présentation, et le public a été super réceptif (merci d'ailleurs à celle qui avait un rire très communicatif). J'ai eu beaucoup de retours positifs à la fin de la session, comme quoi sur un malentendu, un sujet que vous pensez anodin peut aider d'autres !

La vidéo est disponible [ici](https://youtu.be/NHHlKoEdT2M).

##  Comment faciliter les contributions open source et mieux les évangéliser

Pas le temps de se reposer, j'enchaine avec une table ronde sur les contributions Open Source.

J'avais fait une présentation à l'Open Source Experience 2020 à ce propos, et c'est en partie pour cela que Denis Germain aka [Zwindler](https://blog.zwindler.fr/), l'orga des tables rondes, m'a proposé d'y participer. Toujours à relever les nouveaux défis, j'ai accepté l'invitation !

Pendant 30 minutes, nous avons échangé avec [Christophe Chaudier](https://twitter.com/c_chaudier) des Compagnons du DevOps et [Zineb Bendhiba](https://twitter.com/ZinebBendhiba) de Red Hat sur comment faciliter la contribution.

La table ronde, animée par [Laïla Atrmouh](https://twitter.com/leiluspocus) de Ladies Of Code, était très enrichissante car l'on a pu partager nos divers points de vue : en tant que mainteneur de projets, "évangéliste", ou simple contributeur. Nous sommes tous d'accord sur un point : contribuez, ça ne vous apportera que du bon !

Mention spéciale à Zineb, qui parle de syndrôme de l'imposteur alors qu'elle est la plus légitime sur le plateau.

La vidéo est disponible [ici](https://youtu.be/XalAQM1-YMg).

## Comment rendre les secrets Kubernetes vraiment… secrets?

Dernier talk de la journée, [Julie Hourcade](https://twitter.com/louhdetech) de chez Formance souhaitait nous révéler un secret... sur les secrets Kubernetes !

Comment les rendre secrets ? Car par défaut, on ne peut pas dire qu'un encodage base64 soit très sécurisé...

Après avoir fait le tour des solutions possibles, elle nous montre à chaque fois ce qui ne va pas dans ces solutions, et la complexité que cela peut amener.

Cependant, elle nous préconise les solutions les "moins pire", tant sur la partie sécu que la partie complexité, avec une conclusion qui est toujours énervante, mais réaliste : "ça dépend" ;)

![Secrets](/2023/03/2023-03-07-kcdfr-secrets.jpg)

La vidéo est disponible [ici](https://youtu.be/RQqmym_34WY).

### Commentaires

> Belle présentation des solutions existantes, avec leurs avantages et inconvénients.
> Sur la forme, les messages passaient bien, avec une pointe d'humour piquant. Le stress a fait que Julie est allé super vite, le talk a été écourté, mais laissant du coup beaucoup de place aux questions / réponses.

## Goodies

Enfin, une conf tech sans goodies ce n'est pas une conf tech. Autant je reste mesuré et ne prends que quelques stickers pour les enfants en tant normal, autant cette fois-ci j'ai craqué.

Et le prix du goodies le plus utile revient à HA-Proxy avec sa batterie solaire, merci !

Je suis dégouté par contre, ayant complètement zappé le stickers bar de l'espace communautaire :(

![goodies](/2023/03/2023-03-07-kcdfr-goodies.jpg)

## Ils en parlent aussi

Retrouvez deux autres articles de blog à ce sujet :

- [Une journée avec moi au Kubernetes Community Days](https://blog.antoinemayer.fr/2023/03/10/une-journee-avec-moi-au-kubernetes-community-days/) par [Antoine Mayer](https://www.linkedin.com/in/antoine-mayer/)
- [Notes du Kubernetes Community Days France](https://leiluspocus.netlify.app/fr/posts/2023/03/notes-du-kubernetes-community-days-france/) par [Laïla Atrmouh](https://twitter.com/leiluspocus)
- [Ma première expérience d'orga - Retours sur KCD France 2023](https://blog.zwindler.fr/2023/03/11/ma-premiere-experience-d-orga-kcd-france-2023/) par [zwindler](https://twitter.com/zwindler)
- [Cockpit io au Kubernetes Community Days France 2023](https://blog.cockpitio.com/cloud/kubernetes-community-days-france-2023/) par l'équipe de cockpit.io