---
title: "Trouver les images vulnérables à la faille log4j avec bash et trivy dans k8s"
date: 2021-12-12T12:00:00Z
thumbnail: /2021/12/2021-12-12-log4j.png
categories:
  - Sécurité
tags:
  - Bash
  - IT
  - Kubernetes
description: "CVE-2021–44228: Trouver les images vulérables à la faille log4j avec bash et trivy dans un cluster kubernetes."
toc: true
---

Il y a quelques jours, le 10 décembre 2021, une vulnérabilité critique a été découverte sur Apache Log4j 2, une librairie Java pour la gestion des logs. Cette faille permettant l'exécution de code à distance est surnommée Log4Shell.

C'est une bonne opportunité pour jouer avec bash, pour trouver au sein de cluster Kubernetes quelles images de pods sont impactés.

## Lister les images du cluster

Lister les pods est assez facile, mais lister les images est un tout petit peu plus compliqué. Nous devons jouer avec l'option [`jsonpath`](https://kubernetes.io/docs/reference/kubectl/jsonpath/) pour sélectionner l'image.

La commande suivante permet de lister toutes les images de pods s'exécutant dans un namespace:

```shell
$ kubectl get pods -o jsonpath=’{range .items[*]}{.spec.containers[*].image}{“ “}’
```

Ajoutons à cela quelque lignes magiques de bash pour classer, avoir les images des sidecars et dédupliquer la sortie, cela donne le résultat suivant :

```shell
$ kubectl get pods -o jsonpath='{range .items[*]}{.spec.containers[*].image}{" "}' | tr " " "\n" | sort -u
consul:1.3.0
elasticsearch:5.6.13
grafana/grafana:5.1.0
jboss/keycloak:4.5.0.Final
jhipster/consul-config-loader:v0.3.0
jhipster/jhipster-console:v4.0.0
jhipster/jhipster-elasticsearch:v4.0.0
jhipster/jhipster-logstash:v4.0.0
jhipster/jhipster-zipkin:v4.0.0
mariadb:10.5
mongo:4.0.2
nextcloud:20.0.6-apache
postgres:10.4
```

Maintenant que nous pouvons récupérer les images, utilisons trivy.

## Trivy

[Trivy](https://trivy.dev/) est un scanner de sécurité open source développé par la société Aquasecurity. Il scanne vos images, pour y trouver toutes les vulnérabilités "CVE". Ce logiciel est embarqué dans d'autres outils "DevSecOps", comme VMware Harbor, GitLab, et d'autres.

Pour l'installer sur votre OS favori, cela se passe par la : https://aquasecurity.github.io/trivy/v0.21.2/getting-started/installation/.

L'utilisation de trivy est très simple. Par exemple, pour voir les vulnérabilités d'une image lancez la commande suivante :

```shell
$ trivy image — severity CRITICAL elasticsearch:5.6.13
```

Le résultat parle de lui-même:

```shell
$ trivy image --severity CRITICAL elasticsearch:5.6.13
2021-12-12T21:20:06.322+0100    INFO    Detected OS: debian
2021-12-12T21:20:06.322+0100    INFO    Detecting Debian vulnerabilities...
2021-12-12T21:20:06.350+0100    INFO    Number of language-specific files: 1
2021-12-12T21:20:06.350+0100    INFO    Detecting jar vulnerabilities...

Java (jar)
==========
Total: 6 (CRITICAL: 6)

+-------------------------------------+------------------+----------+-------------------+---------------+---------------------------------------+
|               LIBRARY               | VULNERABILITY ID | SEVERITY | INSTALLED VERSION | FIXED VERSION |                 TITLE                 |
+-------------------------------------+------------------+----------+-------------------+---------------+---------------------------------------+
| io.netty:netty                      | CVE-2019-20444   | CRITICAL | 3.10.6.Final      | 4.1.44.Final  | netty: HTTP request smuggling         |
|                                     |                  |          |                   |               | -->avd.aquasec.com/nvd/cve-2019-20444 |
+                                     +------------------+          +                   +               +---------------------------------------+
|                                     | CVE-2019-20445   |          |                   |               | netty: HttpObjectDecoder.java allows  |
|                                     |                  |          |                   |               | Content-Length header to accompanied  |
|                                     |                  |          |                   |               | by second Content-Length header       |
|                                     |                  |          |                   |               | -->avd.aquasec.com/nvd/cve-2019-20445 |
+-------------------------------------+------------------+          +-------------------+---------------+---------------------------------------+
| io.netty:netty-handler              | CVE-2019-20444   |          | 4.1.13.Final      | 4.1.44        | netty: HTTP request smuggling         |
|                                     |                  |          |                   |               | -->avd.aquasec.com/nvd/cve-2019-20444 |
+                                     +------------------+          +                   +---------------+---------------------------------------+
|                                     | CVE-2019-20445   |          |                   | 4.1.45        | netty: HttpObjectDecoder.java allows  |
|                                     |                  |          |                   |               | Content-Length header to accompanied  |
|                                     |                  |          |                   |               | by second Content-Length header       |
|                                     |                  |          |                   |               | -->avd.aquasec.com/nvd/cve-2019-20445 |
+-------------------------------------+------------------+          +-------------------+---------------+---------------------------------------+
| org.apache.logging.log4j:log4j-api  | CVE-2021-44228   |          | 2.11.1            | 2.15.0        | log4j-core: Remote code execution     |
|                                     |                  |          |                   |               | in Log4j 2.x when logs contain        |
|                                     |                  |          |                   |               | an attacker-controlled...             |
|                                     |                  |          |                   |               | -->avd.aquasec.com/nvd/cve-2021-44228 |
+-------------------------------------+                  +          +                   +               +                                       +
| org.apache.logging.log4j:log4j-core |                  |          |                   |               |                                       |
|                                     |                  |          |                   |               |                                       |
|                                     |                  |          |                   |               |                                       |
|                                     |                  |          |                   |               |                                       |
+-------------------------------------+------------------+----------+-------------------+---------------+---------------------------------------+
```

On voit que la vulnérabilité CVE-2021–44228  est présente, nous devons patcher ou mettre à jour dès que possible !

## Bash & Trivy

*Edit (15/12/2021)* : Il existe un plug-in tout fait pour cela ! Voir ici: https://github.com/aquasecurity/trivy-plugin-kubectl

Ok, maintenant que nous avons la liste des images dans notre cluster, que nous savons scanner une image, allons plus loin, et bouclons !

Voici un script bash permettant cela juste ci-dessous. Amusez-vous avec ! Notez que dans l'exemple nous listons les images du namespace courant. Vous pouvez l'améliorer au besoin.

```bash
#!/usr/bin/env bash
RED='\033[0;31m'
NC='\033[0m'

OLDIFS="$IFS"
IFS=$'\n'
VULN=$1

# $1 arg is the CVE number to check
if [ -z $1 ]; then
  echo -e "usage: $0 CVE-NUMBER (i.e: './k8s_vuln.sh CVE-2021-44228')"
  exit
fi

# Check command existence before using it
if ! command -v trivy &> /dev/null; then
  echo "trivy not found, please install it"
  exit
fi
if ! command -v kubectl &> /dev/null; then
  echo "kubectl not found, please install it"
  exit
fi

# CVE-2021-44228
echo "Scanning $1..."

namespaces=`kubectl get ns | cut -d' ' -f 1 | tail -n+2`
for ns in ${namespaces}; do
  echo "- scanning in namespace ${ns}"
  imgs=`kubectl get pods,deployments,daemonsets,statefulsets,jobs,cronjobs -n ${ns} -o jsonpath='{range .items[*]}{.spec.containers[*].image}{" "}' | tr " " "\n" | sort -u`
  for img in ${imgs}; do
    echo "  scanning ${img}"
    result=`trivy -q image --light --no-progress --severity CRITICAL ${img}`
    if echo ${result} | grep -q "$1" ; then
      echo -e "  ${RED}${img} is vulnerable, please patch!${NC}"
    fi
  done
done

IFS="$OLDIFS"
```

![Bash script k8s vulnerability](/2021/12/2021-12-12-check_vuln.png)

## Etapes suivantes

Ceci était un rapide exemple d'utilisation bash, mais vous pouvez utiliser des outils de sécurité plus avancé pour être alerté de telles failles, comme [Sysdig Secure](https://sysdig.com/blog/cve-critical-vulnerability-log4j/), [Snyk](https://snyk.io/blog/log4j-rce-log4shell-vulnerability-cve-2021-4428/) ou d'autres !

## References

- Docker blog post: https://www.docker.com/blog/apache-log4j-2-cve-2021-44228/
- Apache Log4j : https://logging.apache.org/log4j/2.x/security.html