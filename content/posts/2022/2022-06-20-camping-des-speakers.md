---
title: "Camping des Speakers"
date: 2022-06-19T23:59:59Z
thumbnail: /2022/06/2022-06-19-camping-logo.png
categories:
  - Conférence
tags:
  - Camping
  - IT
  - Développement
  - Kubernetes
description: Retour sur les sessions de l'évènement IT qui a chamboulé les normes.
toc: true
---

## Une conf tech dans un camping !

![Logo Camping des Speakers](/2022/06/2022-06-19-camping-logo_color_text.png)

Les 9 et 10 Juin 2022 s'est tenu un événement tech pas comme les autres... 2 jours de conférences dans un camping du Morbihan, a.k.a "Le Camping des Speakers" !

Nous étions 4 ~~ex-collègues~~ copains sur place, profitant de cet instant pour partager pleins de belles choses. Voici nos notes (plus ou moins égales) et remarques respectives.

Nous vous souhaitons bonne lecture !

![After](/2022/06/2022-06-19-camping-after.png)

---

## WebAssembly n'est pas qu'une affaire de frontend

[Benjamin Coenen](https://twitter.com/bnj25) de chez Apollo, société qui bosse sur graphql, nous présente WebAssembly et comment on peut l'utiliser hors du navigateur. En effet, quand on parle WASM, on pense surtout aux navigateurs.

### Historique

WebAssembly arrive en 2015, proposé par le w3c. Gros support par Mozilla, puis Fastly qui y voit un intérêt sur la gestion du backend.

WebAssembly est un langage à piles *(stack)*, on dirait du bon vieil assembleur.

À savoir qu'on ne développe pas en Wasm directement, on utilise d'autres langages qui seront ensuite convertis. Les langages supportés son Rust, Go, C, AssemblyScript, qui permettent de compiler vers Wasm.

Attention pour ceux qui développent en go, wasm ne propose pas de garbage collector.

2 formats de fichiers générés :
- .wasm : format binaire
- .wat : webassembly text

Ce que l'on entend dire c'est qu'il est plus performant que JavaScript mais finalement pas tant que ça.

### Côté backend

On peut utiliser Wasm pour divers cas d'usage côté backend : FaaS, smart contract, modules pour d'autres langages, plugins, etc.

- Acteurs côté FaaS : Suborbital, Wasmcloud
- Acteurs côté blockchain : Ethereum
- Quelques projets backend : Krustlet, Hippo

### Tools

- **wasmer** : pour exécuter du code Wasm, le plus simple à intégrer
- **wasi** : pour avoir les entrées/sorties. Plus rapidement mis à jour car équipe de Fastly derrière & permet "d'ouvrir les vannes" par rapport au côté sandbox de wasm 

Binding wasm : utilisation d'une fonction en Rust compilé en wasm puis récupérée en python par exemple *(voir wit-bindgen & witgen)*

![WebAssembly](/2022/06/2022-06-19-camping-wasm.jpg)

> Sujet orienté très dev, même si ce n'est pas mon sujet de prédilection, cela permet au moins d'en savoir un peu plus des possibilités du langage.
>
> [Rémi](https://twitter.com/rverchere)

> Wasm, Wasi, Wasmer ... Des sujets vus de très (très) loin pour ma part et abordés ici avec beaucoup de détail. Un de ces talk qui donne envie d'en apprendre plus ! 
>
> [Cham](https://twitter.com/its_iamcham)

> Sujet côté dev **mais** dont je vois le potentiel pour tout ce qui est FAAS mais pas que! sujet à suivre avec attention.
>
> [David](https://twitter.com/DavAuff)

---

## Culture, innovation et cartes IGN

Nous débutons les sessions par un évènement en extérieur animé par [William BARTLETT](https://twitter.com/bartlettstarman). Le groupe est divisé en trois équipes de pondération égale avec chacune un objectif et une méthode pour l'atteindre qui lui est propre.

### Première équipe

La première équipe avait pour objectif de trouver un plot signalétique rouge en ayant comme contrainte de planifier l'ensemble du trajet qui sera réalisé au sein du Camping. Aucun détour ne sera toléré.

### Deuxième équipe

La seconde équipe a pour objectif de trouver les plots signalétiques jaunes en ayant comme contrainte de planifier leur parcours des 100 prochains mètres. Des planifications successives de parcours de 100 mètres pouvaient ainsi être faites jusqu'à l'atteinte de l'objectif.

### Troisième équipe

La troisième, dont j'ai fait partie, n'avait **aucun objectif** mais évoluait également avec des contraintes.

Contraintes organisationnelles:
- Un Chef
- Un responsable de la sécurité physique du groupe
- Deux sous-groupes d'explorateurs

Contraintes dans nos actions:
- Les explorateurs doivent rester à portée de vue du Chef
- Le Chef ne doit pas se déplacer seul
- Le Chef peut se déplacer une fois les explorateurs revenus auprès de lui afin de se rendre à un point d'intérêt relevé par un explorateur

### Morale de l'aventure

Les trois équipes ont adopté trois méthodes organisationnelles différentes qui pouvaient être associées à:

1. Cycle en "V"
2. Itérations agiles
3. Mode anarchique

En ayant un objectif précis, nous pouvons passer à côté de belles découvertes imprévues.

> Durant cette activité, William met en valeur l'importance dans le choix organisationnel qui nous permettra de développer notre activité. En ayant un objectif trop spécifique, nous pouvons passer à côté de belles découvertes. Le bon positionnement du curseur entre la recherche de performance et l'ouverture à certaines opportunités est clé dans la réussite. De nombreuses découvertes sont les fruits de projets loupés (ex: [Histoire du Post-It](https://fr.wikipedia.org/wiki/Post-it#Une_utilisation_impr%C3%A9vue)).
>
> [Sébastien](https://twitter.com/davoult)

---

## Notre recette de l’équipe parfaite 🧁

L'équipe de [PIX](https://pix.fr/) avec [Estelle Landry](https://twitter.com/estelandry) et [Yvonnick Frin](https://twitter.com/YvonnickFrin) au fourneaux, nous font un REX sur leur organisation de dev pour aller de l'idée en production, en mode recette de cuisine (et toque sur la tête ;)).

Quelques points d'idées lors de la conf, le tout dans la bonne humeur :

- Pas plus de 2 Epic en même temps pour éviter le contexte switching.
- Modèle double diamant : phase de découverte/ouverture, puis refermer pour meilleure définition
- Grosse phase de discovery, histoire d'être certain que c'est la bonne solution
- Une fois que la solution est choisie, création de User Stories pour lancer le dev.
- Une bonne user story : atomique et terminée
- Découpage le plus petit possible pour du fast delivery (feedback loop and co)
- Toilettage et priorisage (en gros tri et priorité)
- En daily : on parle des tickets et pas chaque dev
- Beaucoup de mob/pair programming
- BUS Factor : rendre les personnes dispensables
- Ce sont les PO qui gèrent les déploiements. Tout se passe via slack pour le déploiement avec des bots
- Paas chez scalingo


![PIX](/2022/06/2022-06-19-camping-equipe_parfaite.jpg)

> Beaucoup de sujets / choix d'orga dans lequel je me retrouve, et d'autres que je découvre (j'aime particulièrement le modèle double diamant !). J'ai même une idée pour mon équipe plutôt infra : faire du mob sur de l'IaC, affaire à tester !
>
> [Rémi](https://twitter.com/rverchere)

---

## Et si on faisait le tour de CORS ? ⛵

Ce *talk* animé par [Cédric Gatay](https://twitter.com/Cedric_Gatay) fait bien sur référence au **mécanisme de restriction d'appel de ressources externes**. Durant 45 minutes, Cédric a pu nous faire une rétrospective sur les raisons de la création de ce mécanisme, puis nous a présenté ses bénéfices, les méthodes pour l'implémenter et son fonctionnement.

Quelques conseils:

- La *Programmation par l'erreur* peut sauver, veillez à bien utiliser la console de votre navigateur pour y récupérer l'ensemble des messages.
- CORS, son rôle permet juste de dire si cette *Origin* peut contacter tel *Origin*.
- Les **cors** n'empêchent pas une requête de sortir, mais de lire le résultat côté backend 
- Il existe 6 entêtes max côté backend 
- **Preflight** (notion importante) et la réponse doivent avoir la même entête
- On peut forcer du cross-origin en indiquant l'attribut dans du code html 
- Content-Security-Policy : au niveau du frontend

![Et si on faisait le tour de CORS ?](/2022/06/2022-06-19-camping-cors.jpg "Sketchnote") [@Marc_DUGUE](https://twitter.com/Marc_DUGUE)

[Les slides](https://docs.google.com/presentation/d/1oW-Fxi7oNPl-GV4g9lymArQLDpqUMIp2BPzeIM6BCsw/edit?usp=drivesdk)


> Sujet à forte technicité pour un profil *OPS*, mais *Cédric* à su rappeler le rôle de **CORS** et son champ d'intervention. Bien qu'un sujet initialement adressé aux profils *DEV* chaque *OPS* a déjà été confronté à une problématique qu'il n'arrivait pas à interpréter.
>
> [Sébastien](https://twitter.com/davoult)

> Ayant découvert ce sujet en entreprise sans réellement le comprendre, ce talk m'a permis d'ouvrir les yeux et d'en apprendre davantage sur les CORS. Un beau tour d'horizon très didactique !
>
> [Cham](https://twitter.com/its_iamcham)

> Sujet également très dev, mais ça permet de mieux comprendre les envois de requête HTTP. Attention les requêtes **CORS** sont liés au navigateur!
> Tips: pour faire du **CORS** avec du curl : `curl -H 'Origin: xxxx' https://XYZ`.
>
> [David](https://twitter.com/DavAuff)

---

## Kubernetes : le coeur de la meule … comme vous ne l’avez jamais vu

Les DevRels en chef que sont [Aurélie Vache](https://twitter.com/aurelievache) et [Sébastien Blanc](https://twitter.com/sebi2706) nous font une interprétation à leur sauce de comment fonctionne Kubernetes.

Prenez un chef d'orchestre, des pokeballs, des glaçons, des box Amazon, mélangez le tout et au final vous vous retrouvez avec une explication des Readiness Probes, allez savoir !!

En trois phrases :
- Epic !
- Et si vous mettez des ReadynessProbe/LivenessProbe vous êtes des gens bien
- Définissez les Requests et Limits sur vos ressources #Kubernetes , vous serez un bon citoyen du Cloud !

![Kubernetes au coeur de la meule](/2022/06/2022-06-19-camping-kubernetes_meule.jpg)

> Façon originale de présenter **Kubernetes** avec comme seuls supports, des boules de jonglage et des boites en carton. Amusant, instructif lorsque l'on ne connait pas le sujet, cette démythification à su ravir les plus joueurs d'entres nous. 😏 
>
> [Sébastien](https://twitter.com/davoult)

>  Sujet sur le fond que je connais assez bien, mais beaucoup moins sur la forme ! C'est un peu comme lorsqu'on va voir un film avec nos acteurs préférés : même si le sujet est commun, on sait qu'on passera un bon moment grâce à l'interprétation. Contrat rempli !
>
> [Rémi](https://twitter.com/rverchere)

>  J'ai découvert que l'on pouvait appréhender un sujet complexe d'une manière plus interactive que part des articles ou des vidéos. La scène était parfaite, les acteurs au top et les spectateurs n'en parlons pas! 😛 
>
> [David](https://twitter.com/DavAuff)

> J'ai adoré cramé au soleil en regardant des ballons, poissons et autres jouets voler à côté de la piscine ... tout en parlant de Kubernetes et en déchiffrant des fichiers YAML physique *(en papier)*. L'esprit du Camping des Speakers résumé en un talk !
>
> [Cham](https://twitter.com/its_iamcham)

---

## Comment j'ai monté le bureau OVH Toulouse après un tweet

Et s'il suffisait d'un Tweet pour changer de vie ? Bon, dis comme ça, cela peut faire penser au synopsis d'un divertissement télévisé. 

Mais, sans pour autant changer radicalement de vie, [Sylvain Wallez](https://twitter.com/bluxte) nous a conté dans ce talk la naissance du bureau OVH à Toulouse *(devinez comment)*, les impacts sur sa vie, et pleins d'autres anecdotes croustillantes résumé ici. C'est parti !

![Tweet Octave Klaba](/2022/06/2022-06-19-camping-klaba.png)

Donc, le talk, en résumé : 

- Tout est parti [d'une réponse à un Tweet d'Octave](https://twitter.com/bluxte/status/568708583740858368) pour déconner
- Échanges en MP *(là, ça devient sérieux)*
- Constitution d'une équipe de 10 personnes pour monter le PaaS IoT *(des copains, contacts avec qui Sylvain avait envie de travailler etc.)*
- Entretien à Roubaix de 9h à 19h *(assez intense !)*
- Sylvain est alors pris à l'essai pendant 2 mois à Roubaix *(de base, il habite à Toulouse)*
- Il apprend que le bureau va être ouvert à Toulouse [via un Tweet d'Octave](https://twitter.com/olesovhcom/status/613777432068362240)
- Mr. Klaba *(le père d'Octave)* souhaite faire une visite des locaux le lendemain ! Sylvain a dû tout organiser en quelques heures *(chapeau)*
- Visite de 7 locaux et prise de décision en 1 matinée
- Petite histoire de la famille Klaba & naissance d'OVH *(très instructif !)*
- À la suite d'une visite d'Octave, Sylvain apprend que l'équipe va être doublée via [un nouveau tweet d'Octave](https://twitter.com/olesovhcom/status/664145976328904705)
- 2 mois plus tard, le projet PaaS IoT est séparé en 3
- Ça n'a pas marché finalement, car le modèle économique s'est révélé incompatible avec la vision d'OVH
- Très bonne expérience et très enrichissant cependant

> Ce talk ne va pas changer radicalement ma vie, mais au vu du speaker, des anecdotes et des infos hyper intéressantes il va sûrement avoir un impact et y faire écho. Merci pour ce beau retour d'expérience Sylvain !
>
> [Cham](https://twitter.com/its_iamcham)

---

## Expérimenter et parler d’accessibilité numérique

[Virginie Ferey Rochefeuille](https://twitter.com/pansybloom) nous fait prendre conscience de la difficulté d'accès numériques avec quelques jeux de construction lego.

Au programme, 3 groupes avec dans chacun 1 personne en situation de handicap : aveugle, sourd, et sans usage des bras.
Chaque groupe doit alors construire une figurine en lego, avec chacun son handicap.

Par cette expérience, on peut se rendre compte du décalage de la perception que nous avons sur le handicap en tant que personne valide, et cela permet aussi de montrer qu'une personne en situation de handicap physique a toute sa tête, inutile de la considérer différente.
C'est facile à dire, on le sait tous, mais dans la pratique cet atelier nous montre que ce n'est pas tout le temps le cas !


![Atelier Accessibilité Numérique](/2022/06/2022-06-19-camping-accessibilite.png)

> J'ai pris le rôle de l'aveugle, et j'ai pu "voir" quelques comportements à mon égard que j'ai trouvé soit inapproprié, soit presque agaçant :
> Quand on me dit "prends la pièce à côté".. je veux bien, mais quel côté ?
> Quand on me prend la main pour me guider, sans mon consentement, je me sens considéré autrement...
> J'ai alors bien pris conscience du rapport que l'on peut avoir avec l'autre qui est différent, très instructif que de se mettre à la place de l'autre pour mieux le comprendre. JE recommande !
>
> [Rémi](https://twitter.com/rverchere)


>  Pour ma part j'ai dû accompagner une personne ayant les informations, mais n'ayant pas tous ses membres pour réaliser la tâche demandée.
>  Ma collègue a dû trouver des subterfuges afin de se faire comprendre et adapter son langage pour faire ce qu'elle souhaitait. Inspirant!
>
> [David](https://twitter.com/DavAuff)

> Quant à moi, je devais accompagner un collaborateur ayant une **perte totale de l'audition** dans la réalisation de la structure. Les barrières étaient nombreuses et nous avons dû adopter un moyen alternatif de communication afin de mener à bien la mission. À travers cette mise en situation, nous prenons conscience qu'il est indispensable d'adapter les postes de travail et de sensibiliser les collaborateurs afin de pouvoir accompagner au mieux les collaborateurs en situation de handicap.
>
> [Sébastien](https://twitter.com/davoult)

---

## Indiana Jones et les aventuriers de la conf perdue

Lors de ce *Talk*, [Benjamin DAUVISSAT](https://twitter.com/bdauvissat) nous a présenté l'une des journées les plus longues de sa carrière. À la fois angoissante, perturbante, voir même terrifiante, cela nous rappelle ce qu'il ne faut pas faire en Entreprise.

- Ne pas perdre le mot de passe **root** du serveur Linux portant toute la **VoIP** de la société
- Ne pas rester sur de vieilles versions de **MySQL (5.1)**
- Ne pas rester dans le déni en sous-estimant le gain de performance apporté par le passage *MySQL 5.1 -> MySQL 8* (Passage de 10h de syncro à 40 minutes)
- Ne pas perdre les **Sources** de ses applications clés
- **Specs** != **Code Source**. Le Code doit être fait à partir des Specs, pas l'inverse.

En cas de doute, connaissez-vous le [Test de Joel](https://fr.wikipedia.org/wiki/Test_de_Jo%C3%ABl) ?

> Benjamin nous rappelle qu'une entreprise peut rapidement se laisser déborder si elle ne conserve pas la maitrise de son patrimoine applicatif. L'entreprise se retrouvera tôt ou tard contrainte de corriger la situation qu'elle a laissée s'envenimer, mais cela se fera dans la douleur et généralement au pire moment.
>
> [Sébastien](https://twitter.com/davoult)

---

## Comment j'ai aidé ma fille à lire avec le machine learning

Speaker : [Vincent Ogloblinsky](https://twitter.com/vogloblinsky)

> Étant papa de deux magnifiques enfants, j'ai à coeur de les accompagner dans leurs apprentissages. Pour moi, une des bases essentielles c'est apprendre à lire. 
> Alors, trouver un talk mélangeant l'aide à l'apprentissage et technologie, il fallait que j'y aille absolument et j'ai été bluffé!
> [Vincent](https://twitter.com/vogloblinsky) est parti de ce même constat et a développé une application mélangeant machine learning et application web.
>
> [David](https://twitter.com/DavAuff)

Résumé : 

- Side project pour sa fille
- ML oriented (avec OpenAI)
- Difficulté à avoir des voix d'enfant et un dataset convenable
    - Il est allé voir ses contacts du 1er et 2nd cercle pour les faire participer à cette expérience en proposant d'enregistrer certains sons clés. Il en a reçu une 50aine. Après cela il lui a fallu les nettoyer afin que ces sons soient en dessous ou égale à la seconde (1 sec.).
    - Grâce à cela il a pu implémenté d'autres outils ex: enregistreur pour les sons...
- Pour la partie ML (Machine Learning) ce dernier a utilisé un outil codé en Python assez connu dans ce milieu-ci : Tensorflow
- Beaucoup de difficulté, mais cela lui a permis de voir énormément de domaines (phonétique, gestion du son, AI/ML etc.)
- Le projet est fonctionnel, sur une "petite base", mais il souhaite pourquoi pas l'étendre et également à d'autres use-cases (personnes étrangères souhaitant apprendre à lire etc.)

![Fusion Syllabique](/2022/06/2022-06-19-camping-machiine_learning.jpg)

> Ce projet m'a carrément bluffé!
> Tous les points y ont été développés:
    > - Comment les sons de la langue française sont composés (alphabet, phonèmes, graphèmes...)
    > - Pourquoi il a fait ce projet
    > - Comment le ML fonctionne pour les sons
> etc.
> Un outil bluffant qu'il a fabriqué en partant de zéro.  
>
> [David](https://twitter.com/DavAuff)

> Un side project en 6 mois avec presque "tout à apprendre", chapeau, vraiment bluffé !
>
> [Cham](https://twitter.com/its_iamcham)

---

## Lead Dev, 3 ans d'xp, et alors ?

Devenir lead dev avec 3 ans d'expérience, est-ce réellement possible ? Non seulement c'est possible, mais [Lise Quesnel](https://twitter.com/QuesnelLise) a donné diverses clés tout au long de son talk sur sa vision du lead dev, son retour d'expérience avec la dimension full-remote imposée par la COVID-19, les difficultés rencontrées ... Tellement pris dedans que je n'ai pu prendre beaucoup de notes !

Notes *(clairement non exhaustives)* : 

- Techlead *(plus axé expertise technique)* **≠** lead dev *(plus axé accompagnement et soft skills)*
- Difficultés rencontrées : 
        - Communication externe 
        - Gestion du temps difficile
        - Savoir "trancher" et prendre la "bonne décision"

> Sujet et REX intéressants, je suis curieux de revoir les slides !
>
> [Cham](https://twitter.com/its_iamcham)

---

## Partage et transmission : ratages et amélioration

Durant 45 minutes, dans un format de *Talk* sans *Slide*, [Cécile FREYD-FOUCAULT](https://twitter.com/cecilefreydf) et [Jordane GRENAT](https://twitter.com/JoGrenat) ont pu nous transmettre les différentes façons de transmission et de partage nos expertises. Que ce soit au travers de mentoring, partage, formation, les formats sont nombreux, mais les pièges le sont aussi.

### Le partage

Le partage consiste à véhiculer le savoir entre ses pairs (personnes de mêmes rangs). 

Plusieurs techniques existent, nous avons notamment longuement échangé autour du Pair-Programming:

- Le Pair Programming permet d' augmenter le savoir (Être et Faire).
- Il est nécessaire d'inverser régulièrement les rôles de chacun dans le pair programming afin de casser les routines et éviter les erreurs
- Il est important d'être attentif aux autres
- Il est intéressant d'avoir une bonne mixité dans les binômes(age, genre, expertise, ...).
- Le Pair Programming est **rentable** pour une entreprise

Mais cette méthode de *Partage* peut apporter quelques problèmes:

- Risque de scission dans les équipes, c'est pourquoi, il est important d'imposer un **roulement** et une **mixité**
- Vouloir atteindre l' **État parfait**

### La transmission

La transmission consiste à véhiculer son savoir-être et savoir-faire auprès de personnes inscrites dans un processus d'apprentissage (Étudiants, Apprennant, Mentee ...)

- Le **Mentor** ne doit pas véhiculer d'idée personnelle ne se reposant sur aucun fondement.
- Ne pas faire de *Copinage* dans un process de **Transmission**, cela posera problème le jour ou un recadrage sera nécessaire.
- En cas de création de supports, ce dernier ne **doit pas être autosuffisant**. Il n'est pas nécessaire de reproduire du contenu existant, mais plutôt de capitaliser et d'apporter une *valeur* complémentaire.
- Le **Tuteur** à un engagement de **protection** vis-à-vis de son apprenant par rapport à son environnement , mais également par rapport à l'apprenant lui-même.
    - Horaire de travail
    - Diversité des taches
    - Complexité des missions

![Camping des Speakers](/2022/06/2022-06-19-camping-partage_transmission.jpg "Partage et transmission : ratages et amélioration")


> Cet échange m'a permis de bien différencier la Transmission et le Partage. Les échanges autour des bonnes pratiques permettent d'éviter les pièges dans lesquels nous tombons malheureusement trop souvent...
>
> [Sébastien](https://twitter.com/davoult)

---

## Les 5 choses que j'aurais aimé savoir plus tôt dans ma carrière

Speaker: [Yohan Lasorsa](https://twitter.com/sinedied)

Ce talk de 15 min fait partie, pour moi, des talks les plus **inspirants** proposé par le @campingspeakers

**Disclaimer**: ici je raconte de ce dont je me suis souvenu, il est possible que tout ne soit pas exactement comme ce qu'il a été raconté.


1. Se [faire/garder] des contacts

    En effet on ne sait pas de quoi la vie sera faite. À travers ce talk [Yohan](https://twitter.com/sinedied)
    nous signalait, qu'après différentes rencontres, il a pu faire évoluer sa carrière. Il l'a faite évolué en étant recommandé par ses amis d'étude, des gens qu'il a cottoyé à travers différentes conférences, etc. 
    
    Ce que je retiens de ce point c'est : soyez bienveillant avec les gens, les personnes inamicaux, ignorer les ! Leur Karma jouera en leur défaveur.

2. Plus facile à dire pardon que demander l autorisation
    
    
    Ici l'intervenant nous remontait qu'il était plus facile de dire **pardon** pour une action réalisée au lieu de demander l'approbation de la hiérarchie. Souvent la hiérarchie met du temps pour décider. Si la hiérarchie refuse : on dit pardon et on enlève, si elle accepte on a gagné du temps pour réaliser la tache. 
    
    Avec ça on "force" la main plus ou moins.
    
    Attention, cela ne s'applique pas à tous les cas de figure 😉
    
> Tips, à chaque fois que vous souhaitez créer quelque chose en amont : Créer un fichier LICENCE.md dans votre repository.
>
> [David](https://twitter.com/DavAuff)

3. Soft skills plus important que la technique 

    C'est un des points les plus importants de ce talk. Qu'importe que vous mettez du temps à comprendre une techno, une manière de faire, du moment que vous évitez d'être un c*nnard et que vous garder une attitude positive, cela pourra permettre de "sauver" une équipe au bord du précipice (burn out)
    
    Lors d'un projet à grosse pression, un nouvel arrivant avait des difficultés à s'intégrer à l'équipe, mais surtout au projet et être prêt. 
    Les jours suivants, un red flag a été levé pour signaler le problème et remonter les difficultés que rencontrait l'équipe à lui faire apprendre la techno et l'intégrer au projet. Les responsables de projets n'ont pas répondu à ce red flag, et l'équipe à continuer à le former. Au bout de quelques mois, la personne ayant du mal à l'époque était arrivée au même niveau technique et organisationnel que ceux du projet. Mais durant ces différentes semaines, il a gardé une attitude positive.
    Quelques semaines plus tard, la pression devenait plus pesante, mais grâce à l'optimisme du nouvel arrivant, qui ne l'était plus, il a su garder cet état d'esprit (être positif, raconter quelques blagues, relativiser) et cela a apporté à l'équipe. À l'aide de son soft skills, ils ont pu finir le projet dans de meilleures conditions.

    En y repensant, la personne ayant remontée le red flag à demandé à ses responsables de ne pas y tenir rigueur, et leur à demandé de l'embaucher pour ses compétences technique mais également ses **soft skills**

4. Pour progresser, sortir de sa zone de confort

    Je ne vais pas forcément développer ce point. C'est un secret de polichinelle. Pour avancer rapidement dans nos technos/métier il faut se mettre en danger et oser! Et si ça ne fonctionne pas, au moins on aura appris des choses.

5. Pas de certitude dans la tech
    
    Tout le monde souhaite utiliser les dernières technos à la mode. Aujourd'hui, d'après l'intervenant, la librairie JavaScript la plus utilisée sur le web c'est JQuery, le langage coté serveur des sites web : PHP. En termes de techno ce sont des dinosaure, mais celles-ci continuent à évoluer et à répondre au besoin. Alors, pourquoi vouloir changer à part pour la curiosité intellectuelle?
    
    Comme l'a dit [Olivier Poncet](https://twitter.com/ponceto91) lors du talk : Si l'application n'est pas cassée ne la répare pas!

![Les 5 choses que j'aurais aimé savoir plus tôt dans ma carrière](/2022/06/2022-06-19-camping-choses_carriere.png "Yohan Lasorsa en Tong")

> À la fin de ce talk qui devait durer 15 minutes mais qui a durée au final 25 minutes, nous avions pu échanger de manière informelle et en maillot de bain lunette bière. c'était vraiment une bonne expérience.
> Ce que j'en retiens de ce talk:
    - Oser, quitte à se tromper
    - Les erreurs font partie de l'apprentissage
    - Être bon dans les technos oui, mais avoir ce petit plus qui peut faire avancer son équipe mieux!
    - Être bienveillant et ouvert aux autres et ils vous le rendront 
>
> [David](https://twitter.com/DavAuff) 

---

## Comment (ne pas) être un c*nnard en 5 leçons 🤗

La journée a été longue, inspirante, avec des talks techniques, d'autres moins, certains décalés.

L'heure passe, place maintenant à des sujets plus intimes, plus de confidences... Et nous ne sommes pas en reste avec [Valériane Venance](https://twitter.com/valeriane_IT) et [Noël Macé](https://twitter.com/noel_mace), qui prennent sur eux pour nous parler de comportements de c\*nnard envers les autres.

Au travers d'un dialogue entre 2 individus, ils placent subtilement pas mal de réflexions, clichés, comportements qu'on peut avoir envers les femmes, les minorités diverses et variées, le tout avec un fond de vécu et exemple concrets.

On y parle notamment de "manterrupting", "manspaining", "not all X" et l'analogie du sandwich, avec les violences systémiques, et plein d'autres termes pas forcément joyeux à entendre, mais cela fait du bien de remettre en place les choses. On y apprend aussi que tout le monde peut agir contre les c\*nnards, éventuellement en être un sans le savoir et donc corriger le tir, et qu'on n'est pas obligé d'être visé par eux pour agir (oui, ça fatigue de toujours répéter les mêmes choses).

Dans un second temps, place à l'échange avec le public, dont beaucoup se retrouvent dans les paroles du duo, et osent parler en public du harcèlement qu'ils peuvent subir au quotidien.

![Comment (ne pas) être un c*nnard en 5 leçons](/2022/06/2022-06-19-camping-lecons.jpg)

> Je n'en ai pas honte, mais ça ma amené à réfléchir, et prendre les actions nécessaire et concrète, pour ne plus être un c\*nnard. Le talk sous forme d'un échange au sujet de ce même talk nous amène à une réflexion sur soi et sur ce qui nous entoure. 
>
> [David](https://twitter.com/DavAuff)

> J'ai voulu apprendre à ne plus être un c\*nnard. J'ai appris que je pouvais l'être d'une manière bien différente de ce que j'imaginais. Un talk d'exception qui ouvre les yeux sur pas mal de sujets !
> [Cham](https://twitter.com/its_iamcham)

> Dans ce *Track* engagé, Noël et Valériane ont su traiter de sujets sensibles avec un esprit critique et constructif. L'objectif était de nous sensibiliser sur des sujets de société dans lesquels nous ne pouvons pas rester spectateurs.
>
> [Sébastien](https://twitter.com/davoult)

> Sujet très délicat à aborder, mais cela fait du bien (si on peut dire cela) de pouvoir parler librement de ces sujets. En tant qu'homme blanc, hétéro, cadre, je ne suis pas forcément minoritaire, mais j'ai pu voir que cela ne m'empêchait pas d'agir, certain.e.s sont fatigués d'expliquer toujours les mêmes choses, on peut prendre le relais. Bravo en tous cas !
>
> [Rémi](https://twitter.com/rverchere)

---

## Améliorer les compétences et les infrastructures avec les katas d'architecture

Vous connaissez les katas au karaté ? Savez-vous que vous pouvez aussi transposer cette activité pour affiner vos compétences *(et vos infrastructures)* grâce à l'expérience cumulée de vos collègues et vous-même ? 

[Alexandre Touret](https://twitter.com/touret_alex) dans son talk l'explique très bien et invite tout à chacun à se questionner sur cette discipline intéressante à intégrer *(selon lui)* dans toute formation interne d'architecture logicielle. Car après tout : 

> "Comment sommes-nous censés avoir de grands architectes, s'ils n'ont la chance d'être architectes que moins d'une demi-douzaine de fois au cours de leur carrière ?" 
>
> [Ted Neward]()]     

![Katas](/2022/06/2022-06-19-camping-kata_architecture.jpg)

Par ici les [slides](https://speakerdeck.com/alexandretouret/ameliorer-les-competences-et-les-infrastructures-avec-les-katas-darchitecture-cc629dfb-87f6-4033-aabd-472472ae5655) !


> Modèle C4 intéressant et principe de katas tout autant, à creuser, très clairement !
>
> [Cham](https://twitter.com/its_iamcham)

---

## Depuis 2 ans, je suis la seule à lire mes mails ! (ou presque)

Durant le *Talk* autour de l'utilisation de solutions mél respectueuses de la vie privée, *Morgane Troysi* nous présente le fruit de ses recherches d'une solution alternative à **GMail**.
Au travers d'une matrice comparative, Morgane a comparé *Proton Mail*, *Posteo*, *Mailfence* ou encore *Tutanota* sur des critères comme chiffrement, taille de boite, protocoles, publicité et bien sûr, le prix.

![](/2022/06/2022-06-19-camping-mails.jpg "Depuis 2 ans, je suis la seule à lire mes mails ! (ou presque)") [@Marc_DUGUE](https://twitter.com/Marc_DUGUE)

> Dans ce Talk traitant du respect de nos libertés individuelles, Morgane a su mettre en lumière que des solutions alternatives au Géant qu' est Google Mail existent. Il aurait été intéressant d'ouvrir plus largement le débat en traitant de notre dépendance aux services des **GAFAM**, peut-être un sujet pour la deuxième édition ? :smirk: 
>
> [Sébastien](https://twitter.com/davoult)

---

## Développer un opérateur Kubernetes en Java, c'est possible ! 

Sous la forme d'une [démo](https://github.com/philippart-s/camping-java-operator) très complète [Stéphane Philippart](https://twitter.com/wildagsx) nous a montré comment créer un opérateur Kubernetes en [Java](https://javaoperatorsdk.io/) !


![baby DevRel](/2022/06/2022-06-19-camping-java_operator.jpg)


> Très peu de choses à dire pour ce talk vu que la démo était bluffante et le speaker très passionné. J'ai été pris dedans, et ça m'a donné envie de "ressortir mon Quarkus" pour créer un opérateur k8s !
>
> [Cham](https://twitter.com/its_iamcham)

> Pour ma part, j'ai découvert ce que le SDK Java & Quarkus peuvent réaliser et j'ai vraiment été impressionné en termes de technicité. Le speaker a super bien préparé son talk, et la documentation du projet est tout simplement parfaite !
>
> [David](https://twitter.com/DavAuff)

---

## La potion magique pour faire progresser ta carrière

[David Pilato](https://twitter.com/dadoonet) présente à sa manière tous les bons conseils qu'il a pu avoir pour progresser dans la tech depuis ses débuts.

Vous retrouverez son transcript [ici](https://david.pilato.fr/blog/2022-06-10-la-potion-magique-pour-faire-avancer-ta-carriere/), ce sera plus simple ;)

**Spoiler** : l'OpenSource y est pour beaucoup !

![La potion magique pour faire progresser ta carrière](/2022/06/2022-06-19-camping-carriere.jpg)

> 100% en phase avec lui, c'est toujours cool de voir qu'on est sur la même longueur d'onde avec des personnes comme David, qui rayonnent sur la tech fr. J'avais fait un talk complémentaire à ce sujet à l'[OpenSource Experience 2021](https://www.youtube.com/watch?v=yVOxMlCXyGs).
>
> [Rémi](https://twitter.com/rverchere)

---

## Quand 'Les trois petits goldies' jouent dans le zoo de Capitain kube (édition slasher)

Un collègue de meetup [Jérôme Masson](https://twitter.com/sphinxgaiaone), nous explique les bonnes pratiques pour avoir une application résiliente dans un contexte Kubernetes.

On parle d'Affinities, de NodeSelector, de Chaos Engineering... mais le tout adapté avec la fameuse histoire des 3 petits cochons !

![Quand 'Les trois petits goldies' jouent dans le zoo de Capitain kube (édition slasher)](/2022/06/2022-06-19-camping-kubernetes_chaos.png)

> Belle mise en scène du sujet, avec exemple concret et explications à la clé ! À revoir avec une vraie connexion Internet (et oui, en mode camping on fait avec les moyens du bord !)
>
> [Rémi](https://twitter.com/rverchere)

> L'histoire des "trois petits cochons" revisitée pour parler Kubernetes et best pactices pour les déploiements d'application : Super bien amené! **Attention** au Slasher! 
>
> [David](https://twitter.com/DavAuff)


> Belle histoire, beau REX suivis d'échanges très sympas. Manquaient plus que le feu et la nuit noire !
>
> [Cham](https://twitter.com/its_iamcham)

---

## Mission impossible : créer son pipeline CI/CD en quelques minutes

Speaker : [Thomas Boni](https://twitter.com/thomas_boni)

[Thomas Boni](https://twitter.com/thomas_boni) co-créateur de la plateforme r2devops. Plateforme de partage de fichier de Continious Integration et de Best Practices autour de Gitlab & Github.

Durant les 20 min de la conférence [Thomas](https://twitter.com/thomas_boni) nous a parlé de la partie pipeline sur Gitlab, non pas en faisant du slideware mais en nous montrant le tout en live!
Une présentation rapide au sujet de `.gitlab-ci.yml` et de sa documentation (coucou [Philippe Charrière](https://twitter.com/k33g_org)).
Après cela une démonstration en live nous a permis de mieux appréhender l'utilité et tous les mécanismes autour de la CI Gitlab.

Cet outil permet, en rentrant dans votre Gitlab *(on premise/saas)*, de générer des CI à la volée.

**Gitflow != Github Flow**

**Tools** :
- gitleaks
- git flow
- github flow
- r2devops
- mkdoc

On retrouve l'ensemble du travail sur GitLab : https://gitlab.com/r2devops/hub

![R2devops](/2022/06/2022-06-19-camping-r2devops.jpg)

> Speaker qui maîtrise son sujet, au top!
> Un superbe outil à suivre sur.....git....sur Gitlab bien sûr!
>
> [David](https://twitter.com/DavAuff)

> Je trouve l'idée de la market place de jobs de CI/CD super sympa, et c'est simple mais taguez vos templates c'est toujours utile !
>
> [Rémi](https://twitter.com/rverchere)

---

## Comment bien foirer vos certifications Kubernetes CK{A,S}

Durant cette session de retour d'expérience [Rémi VERCHERE](https://twitter.com/rverchere) nous a présenté en dix points clés **comment bien louper ses certifications** Kubernetes.

1. Trop de confiance
2. Ne pratiquez pas
3. N'apprenez ni `vim`, ni `kubectl`
4. Ne vous entrainez pas sur *killer.sh*
5. Ne faites pas de *Bookmarks* sur *kubernetes.io*
6. Utilisez votre 1er essai le plus tard possible
7. Oubliez que vous avez souscrit à l'examen
8. Ne vous mettez pas dans les conditions de l'examen
9. Ne lisez pas les questions en entier
10. N'utilisez pas les contextes K8s


![Aahh Remi!](/2022/06/2022-06-19-camping-kubernetes_certif.jpg)

> Ce retour d'expérience par l'échec permet de nous rappeler qu'une excellente connaissance technologique n'est pas toujours suffisante pour décrocher nos certifications. Les préparatifs doivent être sanctuarisés afin d'être le mieux armés pour l'examen, quitte à surperformer le jour J, ce sera l'occasion de vous faire payer une bière par vos amis !
>
> [Sébastien](https://twitter.com/davoult)

> Il foire ses certifications Kubernetes, mais il n'a pas foiré son talk ! Les 10 commandements cités sont à imprimer et à donner à tout ceux qui souhaitent passer leurs certifications k8s. Et encore une fois, "Go Rémi, Go !" ;\)
>
> [Cham](https://twitter.com/its_iamcham)

> Si vous suivez ce qu'il vous dit c'est sûr que vous allez foirer vos certifications/exams, faites le contraire et vous risquez de les avoir 😉
>
> [David](https://twitter.com/DavAuff)

---

## Développer une culture Tech engagée - retour sur lbc², première conférence tech par leboncoin

Durant une vingtaine de minutes [Guillaume GRILLAT](https://twitter.com/grillatg) nous a présenté comment avec **Le bon coin** ils ont pu libérer la parole sur des sujets de sociétés tels que le réchauffement climatique mais aussi de l'inclusion de la communauté LGBTQ+.

lbc2.fr : La plateforme de conf du bon coin focus sur la tech
 
 ![Parler de Tech Engagé](/2022/06/2022-06-19-camping-lbc2.jpg)
 
> *Le Bon Coin* est un acteur majeur en France dans la fourniture de services aux utilisateurs. C'est près de la moitié des Français qui parcours leur site à la recherche du bon *Bien* ou *Service*.
> Pour absorber cette charge importante, Guillaume nous a présenté l'organisation qu'ils ont dû adopter et notamment les **70 Features Teams** qui leur permettent de pouvoir être efficients dans leurs *MEP*.
>
> [Sébastien](https://twitter.com/davoult)

> L'entreprise d'aujourd'hui n'est plus une entité où l'on fait juste son travail. C'est un lieu où passe la majeure partie de ses journées et où l'on croise des personnes de différentes origines, orientation, etc. 
> L'entreprise a un rôle sociétal. C'est pourquoi  [Guillaume GRILLAT](https://twitter.com/grillatg) est venu nous présenter ce que fait *Le Bon Coin* en termes d'inclusivité et son rôle au sein de cette société.
>
> [David](https://twitter.com/DavAuff)
 
> C'est aussi une des forces du Camping des Speakers, aborder d'autres sujets, tout aussi importants que notre chère tech. J'ai beaucoup aimé les "petites histoires" de Guillaume et les divers tips pour engager des actions concrètes afin que chacun puisse libérer sa parole. Inspirant !   
>
> [Cham](https://twitter.com/its_iamcham)

---

## Articles relatifs à l'évènement

- [Lucien BILL - Le Camping des Speakers - ❤🎉🤯🤿🤩](https://www.lucienbill.fr/Camping/)
- [Alexis LOZANO - Le Camping des Speakers 2022](https://alexis-lozano.com/le-camping-des-speakers-2022-1/)
- [Stephane PHILIPPART - Mon Camping des Speaker 2022](https://philippart-s.github.io/blog/articles/conf%C3%A9rences/camping-speakers-2022/)
- [Jordane GRENAT - 🇫🇷 Camping des speakers : 1ère édition inoubliable !](https://www.grenat.eu/blog/camping-speakers-inoubliable/)

---

## Les Auteurs

Vous pouvez retrouver les auteurs :

- Sébastien Davoult : [@davoult](https://twitter.com/davoult)
- Chamseddine Saadoune dit "Cham" : [@its_iamcham](https://twitter.com/its_iamcham)
- David Auffray : [@DavAuff](https://twitter.com/DavAuff)
- Rémi Verchère : [@rverchere](https://twitter.com/rverchere)
