---
title: "Notes Devoxxfr 2022 - Feedback"
date: 2022-04-23T23:59:59Z
thumbnail: /2022/04/2022-04-21-devoxxfr-jeudi_00.jpg
categories:
  - Conférence
tags:
  - DevoxxFR
---

Après 2 jours intenses de conférence et plus, un feedback sur ma première conférence #DevoxxFR, spéciale 10 ans, et j'espère que ce ne sera pas la dernière !!

![](/2022/04/2022-04-22-devoxxfr-vendredi_04.jpg)

Retrouvez le récap des 2 journées ici :
- [Jeudi 21 avril 2022](/posts/2022/2022-04-21-devoxxfr-jeudi/)
- [Vendredi 22 avril 2022](/posts/2022/2022-04-22-devoxxfr-vendredi/)


## Organisation

Tout d'abord, parlons orga : un grand BRAVO aux gens en rouge, qui ont géré l'organisation, c'était simplement parfait.

On voit qu'il y a 10 ans d'expérience pour gérer une telle machine:
- Gestion des confs & timekeepers super : j'ai toujours pu assister aux conférences que j'avais sélectionnées (il faut un minimum d'organisation quand même en arrivant à ce genre de conférence)
- Stands: il y avait quand même pas mal de monde autour des stands, surtout en milieu de matinée. J'ai bien fait de me lever tôt pour faire le tour avant les confs
- Pauses repas sans encombres : même s'il y avait de longues files d'attentes, c'était fluide et je n'ai pas perdu de temps pour assister aux autres confs

Aucun soucis à déclarer de ce côté là, je n'ai jamais eu à aller réclamer quelque chose... en général c'est bon signe !

Ma boite organisant les [Devops DDay](https://www.devops-dday.com) sur Marseille, je sens qu'on va piquer des idées ;)

Je ne connais pas personnellement les organisateurs, alors je dirai simplement un grand merci à l'ensemble de la team, continuez ainsi !

## Conférences

Sur un événement de ce style, on a le choix dans les conférences !!

J'avais peur d'avoir des sujets trop orienté dev, finalement la quasi totalité des confs retenues m'ont intéressé, encore un bon point !

Je remarque aussi qu'étrangement, les conférences que j'ai le plus apprécier ne sont pas forcément celles auxquelles je m'attendais.

Belle transition alors sur mon **top 3** des conférences, ci après.

### Top 3

1. "**Les lois universelles de la performance**" par [Raphaël Luta](https://twitter.com/raphaelluta)

"Bluffé" par l'approche des modèles mathématiques présentés, j'ai été très agréablement surpris. Je ne pensais pas être aussi captivé par le sujet, qui était abordé de manière très pragmatique, encore bravo !

2. "**Qu'avons nous appris après un an passé à développer des opérateurs Kubernetes ?**" par [Etienne Coutaud](https://twitter.com/etiennecoutaud)

Un sujet rondement mené par Etienne, qui a su être didactique sur les composants d'un opérateur. Opération pas évidente.

3. "**Montée de version sans interruption**" par Nelson Dionisi

La encore un REX bien expliqué, avec un sujet que je ne pensais pas traité de cet angle (maj BDD)

Etrangement pas de sujets d'observabilité dans mon top 3, mais ces sujets étaient quand même top !

###  Mentions spéciales 

1. "**Dans les coulisses du 'Cloud'**" par [Cécile Morange](https://twitter.com/AtaxyaNetwork)

Franchement, si tous pouvaient avoir la gniaque et expertise à ton âge, le monde se porterait bien mieux ! Ok il y avait beaucoup de stress senti lors de la présentation, mais OSEF le message est passé !

2. "**Simplifiez vos revues de code avec le rebase interactif**" par [Sonia Seddiki](https://twitter.com/sonia_seddiki)

Un quickie beaucoup trop court, Sonia était très didactique sur un sujet qui ne vend pas forcément du rêve. Pour une première conf c'était plutôt très bon !

## Rencontres, Meet & Greet, etc.

Devoxxfr c'est des confs, de la tech, mais aussi des rencontres.

J'ai pu rencontrer des (futurs) collègues d'Accenture, clients, fournisseurs, d'anciens qui - comme moi - ont eu plusieurs vies pros et se retrouvent à Devoxxfr.

J'en ai profité pour rencontrer IRL de nombreuses personnes avec qui j'échange sur twitter, notamment [@zwindler](https://twitter.com/zwindler), [@damyr_fr](https://twitter.com/damyr_fr), [@louhdetech](https://twitter.com/louhdetech), [@aurelievache](https://twitter.com/aurelievache), [@ponceto91](https://twitter.com/ponceto91), [@k33g_org](https://twitter.com/k33g_org), [@idriss_neumann](https://twitter.com/idriss_neumann). C'est un plaisir d'échanger avec vous, même si c'est toujours un peu trop court.

On finit surtout par quelques goodies, des t-shirts, stickers pour les enfants qui sont toujours ravis de me voir revenir pleins de cadeaux inutiles :P

## La suite

J'espère bien remettre ça l'an prochain, mais l'année va être chargée, d'autres conférences vont arriver, des CFPs à remplir pour moi aussi partager les quelques trucs que je pense être utiles à diffuser.

Encore un grand bravo à l'équipe, et à l'année prochaine j'espère !
