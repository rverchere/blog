---
title: "Comment bien foirer vos certifications Kubernetes CK{A,S}"
date: 2022-01-24T23:59:59Z
thumbnail: /2022/01/2022-01-24-comment-bien-foirer-vos-certifications-kubernetes_01.png
categories:
  - Retour d'Experience
tags:
  - Kubernetes
---

En début d'année 2021, j'ai tenté la Certification Kubernetes Administrator (CKA), et j'ai **échoué** au premier essai.

Cette année (2022), j'ai également tenté la Certification Kubernetes Security Specialist (CKS), et j'ai à nouveau **échoué** au premier essai !

![CKA/CKS Failure](/2022/01/2022-01-24-comment-bien-foirer-vos-certifications-kubernetes_01.png)

Avec ces expériences acquises sur le "fail" de certifications, je vous propose quelques astuces pour mettre toutes les chances de votre côté et échouer ! Lisez alors avec attention ce qui suit, à vos risques et périls !

## Avant / Préparation de l'examen

### Trop confiant

Nous étions quelques-uns à se motiver pour passer la certification. Quelques amis et collègues ont réussi l'examen haut la main, même certains dont je ne savais *même pas* que le sujet les intéressait. Si eux ont réussi, pourquoi pas moi ? En plus, ils n'ont jamais utilisé Kubernetes en production !

Premier constat : OK, ce ne sont peut être pas des experts, mais ils ont travaillé dur, bossé consciencieusement, et sont peut-être aussi habitués à passer ce genre de certifcations, pas moi ! C'est un bon point de départ si vous vous considérer suffisamment bon pour ne pas réviser et être studieux, c'est un bon début pour prévoir l'échec !

### Ne pratiquez pas

La CKA et CKS sont des examens de **mise en situation** avec de la **pratique**. Entre 15 et 20 questions de type lab, pas de QCM ici. Vous devez alors être à l'aise avec la CLI et les commandes **kubectl**, comme par exemple:
- créer un `pod`
- mettre à l'échelle un `deployment`
- configurer l'`api-server`
- créer des `network-policies`
- éditer un **paquet** de fichiers `yaml`
- etc.

Si vous ne savez pas le faire instinctivement, tant mieux, quelques recherches sur Internet pour savoir de quoi on parle devrait suffire

### N'apprenez pas vim ni les raccourcis kubectl

Pour être plus efficace, il existe des raccourcis sous `vim` et `kubectl`, qui vous font gagner un temps précieux (indentation yaml, forcer l'arrêt des conteneurs, etc).

Egalement, vous pouvez générer des manifests `yaml` avec l'option `--dry-run` pour avoir déjà une base pré-remplie.

Vous ne devez pas les utiliser, il ne faudrait pas que vous gagniez du temps sur la suppression de ressources, ou l'édition de fichiers.

### Ne vous entrainez pas avec killer.sh

Depuis l'été 2021, la Linux Foundation offre 2 examens blancs gratuits sur la plateforme  [killer.sh](https://killer.sh), qui sont plus difficiles que l'examen réel. Voir le blog post [ici](https://training.linuxfoundation.org/blog/linux-foundation-kubernetes-certifications-now-include-exam-simulator/) pour plus de détails.

Ces 2 sessions sont les mêmes, et vous avez 36 heures pour chacune d'entre elles pour profiter d'aller chercher un maximum d'informations et détails sur les questions posées, et vérifier que tout fonctionne comme vous l'avez compris. Les résponses sont explicitées si vous n'y arrivez pas.

Ne l'utilisez surtout pas alors, vous pourriez apprendre des choses intéressantes !

### Ne faites pas de bookmarks sur Kubernetes.io

Dans les directives de l'examen, il est possible d'avoir un onglet supplémentaire de votre navigateur ouvert pour rechercher de la documentation et exemples sur le site officiel [kubernetes.io](https//kubernetes.io). Ne l'utiliez simplement pas.

Bon OK, ça vous démange, vous êtes quand même curieux, vous voulez y jeter un oeil. Ça me va, mais assurez-vous de ne pas avoir parcouru le site ni prévu de bookmarks sur les éléments clés de la certification (vous trouverez quelques bookmarks déjà tout prêts sur Internet, mais ne les utilisez pas vous risqueriez de réussir).

### Utilisez votre 1er essai le plus tard possible

Une fois la certification achetée, vous avez 1 an pour la passer, et 2 essais. Le 2ème essai doit être passé dans l'année. Vous pouvez alors tenter une première fois la certif, échouer, la retenter une deuxième fois, et échouer à nouveau, cela fait un beau combo !

Pourquoi se faire autant de mal, et simplement échouer définitivement lors du premier essai, en passant la certification le plus tard possible ? Mon conseil : planifier la date du premier essai le plus tard possible, ainsi il vous sera impossible d'utiliser votre "free retake". Quoi de plus facile ?

### Oubliez que vous avez souscrit à l'examen

Cell-ci est facile: achetez un bon pour passer la CKS, travaillez sur pleins d'autres sujets qui n'ont rien à voir pendant l'année, et d'un coup recevez un mail qui vous informe qu'il ne vous reste que quelques jours pour passer la certif !

![CKS Mail](/2022/01/2022-01-24-comment-bien-foirer-vos-certifications-kubernetes_04.png)

C'est ce qui m'est arrivé... Seulement 1 mois pour préparer l'examen, en période de fêtes de fin d'année, avec le travail quotidien, la gestion familiale, et le 2ème essai inclus !


### Ne vous mettez pas dans des conditions d'examen

La CKA a été mon premier examen de la vie professionnelle après mes diplômes universitaires, "quelques" années plus tôt. J'ai oublié comment cela pouvait être stressant. Quelques éléments ci-après pour ne pas vous mettre en conditions de passsage d'examen :

* Ne dormez pas, ou peu, et faites tout ce que vous pouvez pour être le plus nerveux possible.
* Ne nettoyez pas votre bureau / pièce, étant donné qu'on vous le demandera le jour J
* Utilisez une webcam la plus bas de gamme possible, ainsi l'examinateur ne pourra pas vérifier votre identité
* Prévoyez une journée de travail arrassante la veille (une bonne mise en prod bien pourrie) afin d'être le plus fatigué possible (essayez aussi de planifier l'exam tard le soir, quite à être fatigué...)
* Si vous avez des enfants, en période COVID faites en sorte qu'ils soient malades pendant vos révisions et examen (*ce n'est pas une blague, ça m'est arrivé !*)

![I will fail, this is fine](/2022/01/2022-01-24-comment-bien-foirer-vos-certifications-kubernetes_02.png)

## Pendant l'examen

Ca y est, on y est, c'est *LE* jour **J**, plus que quelques instants et vous allez le rater cet examen ! Dans le cas où vous n'êtes pas 100% sûr de vous, quelques conseils supplémentaires ci-après.

### Ne lisez pas les questions en entier

Parfois, des indications vous facilitant la vie peuvent être écrites à la fin des questions. Le sachant, surtout évitez de les lire, faite ce qu'on vous demande ligne après ligne.


### N'utilisez pas les contextes k8s

Pour chaque question, on vous indique sur quel cluster il faudra intervenir, avec des contextes différents. Par exemple, on vous demande de corriger un déploiement sur le cluster B, mais vous étiez sur le cluster A lors de la question précédente.

Si vous ne changez pas de contexte, vous aurez 100% de chance de ne pas trouver le déploiement qui pose problème !

Cela parait **trivial**, mais j'ai perdu **beaucoup** de temps à cause du changement de contexte lors de ma première tentative CKA (*oui, j'étais particulièrement stressé*).

### Ne sauvegardez pas votre travail

Lorsqu'on vous demande de créer des déploiements ou tout autre ressource kubernetes, vous pouvez générer le manifest `yaml` pour l'adapter, le rejouer plus tard, si jamais vous vous êtes trompé et que vous souhaitiez corriger le tir.

Inutile de garder ces fichiers dans un coin, pourquoi revenir sur quelque chose qu'on pense faux, ce sera ça en plus pour louper la certif.

### Prenez votre temps

L'examen dure deux heures. Ce n'est pas la course, prenez votre temps. En étant optimiste, l'examen comporte 15 questions, et vous devez avoir un score d'au moins 68%... Soit au minimum 11 questions justes. Sur 2 heures, cela revient à 11 minutes par question, vous êtes larges !

### Lorsque c'est terminé, arrêtez tout, tout de suite

Dans le cas ou vous n'auriez pas pris en compte le conseil précédent, que vous avez répondu à toutes les questions et donc il vous reste du temps, n'en perdez pas d'avantage ! Ne vous relisez pas, ne retestez pas vos manifests, c'est inutile. Demandez simplement à l'examinateur de clore la session le plus vite possible, et fuyez !

## Fin de l'histoire

Avec tous ces tips, j'espère que vous échouerez aussi bien que moi. Cela dit, si vous ne voulez pas, il suffit simplement d'appliquer l'inverse de ce que j'ai écrit précédemment ;)

Heureusement, après toutes mes erreurs et échecs, j'ai été plutôt contrarié et frustré. J'ai alors décidé d'utiliser mon deuxième essai la semaine suivant ma première certification : j'étais déjà dans des conditions d'examens, je sortai de plusieurs jours d'apprentissage et révisions intensifs, et je savais à peu près où j'avais échoué et j'ai pu travailler sur les sujets que je ne maitrisais pas assez ou n'étais pas assez clairs

Je peux alors résumer mes échecs ainsi pour chaque certification :

1. **CKA**: Pas assez de préparation et entrainements en CLI. Aussi une surdose de confiance en moi pour la CKA, celle-ci étant ma première certif depuis l'université

2. **CKS**: Pas assez de préparation (j'insiste) et de temps, la période pour passer l'examen arrivait à expiration assez vite, et je savais pertinament que j'allais la rater. Mais j'ai pris ce premier essai comme une opportunité, pour me préparer le mieux que possible, pour ensuite réussir au deuxième essai.

Ainsi, je peux le dire maintenant, je suis certificé CKA et CKS, et tout cette préparation en vallait le coup !

![Badges CKA et CKS](/2022/01/2022-01-24-comment-bien-foirer-vos-certifications-kubernetes_03.png)

## Ce que j'ai appris (conclusion)

Evidemment, j'espère que vous ne prendrez pas mes conseils au pied de la lettre, et ferez l'inverse de ce que j'ai écrit si vous souhaitez réellement réussir vos examens CK{A,S}.

Ces échecs sont parfois nécessaires, nous rappelant qu'on peut se tromper. Mais apprendre de ces échecs, cela fait partie de la vie ! Ne vous sentez pas inférieurs lorsque vous lisez toutes ces success-stories sur les réseaux sociaux, tout le monde échoue un jour où l'autre !

Mes derniers mots seront de vous souhaiter bonne chance pour vos certifications. J'espère que vous n'échouerez pas, enfin, pas trop ;)

## Post Scriptum

Depuis l'écriture de cet article, et le passage réussi de la CKS, j'ai profité d'un bureau bien rangé, d'une mise en condition d'examen et d'un week-end tranquille sans les enfants pour tenter la CKAD : cette fois-ci je l'ai eu du premier coup, avec un score honorable de 98% ;)

![Résultat CKAD](/2022/01/2022-01-24-comment-bien-foirer-vos-certifications-kubernetes_05.png)
