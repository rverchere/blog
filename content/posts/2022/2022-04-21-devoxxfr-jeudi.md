---
title: "Notes Devoxxfr 2022 - Jeudi"
date: 2022-04-21T23:59:59Z
thumbnail: /2022/04/2022-04-21-devoxxfr-jeudi_00.jpg
categories:
  - Conférence
tags:
  - DevoxxFR
---

Pendant trois jours s'est tenue sur Paris la conférence DevoxxFR, qui fétait cette année ses 10 ans.

10 ans, mais une première pour moi !

J'ai alors saisi l'occasion pour démarrer ce blog plutôt orienté technique, afin de contribuer à ma manière à la communauté tech française.

Vous trouverez ci-après mes notes des 2 jours de conférences (la 1ère journée étant réservée à des sessions plus longs, auxquelles je n'ai pas pu assister).

L'ensemble des notes sont plutôt "brutes", je laisserai ainsi plutôt que de trop rajouter de bruit au texte.

Vous trouverez également des émoticones suivants pour accéder aux présentations, ressources ou vidéos :
- 💻 : slides
- ⌨️ : ressources (code, exemples, etc.)
- 📽️ : vidéos

![](/2022/04/2022-04-21-devoxxfr-jeudi_01.jpg)

## Début de journée

La journée démarre dès 7h30, je profite pour faire un peu le tour des stands, c'est le calme avant la tempête. Je ne tarde pas trop car à 8h30 je suis invité au petit-déjeuner GitLab non loin de là.

Premières rencontres avec les copains de twitter, on discute un peu, rencontrons devrels & personnes tenant les stands. Je vais faire un coucou aux futurs copains d'Accenture, et échange avec les lèves-tôt des stands voisins.

Quelques goodies et autres discussions plus tard, il est temps d'aller voir les nombreuses conférences, j'ai déjà loupé les keynotes !

![](/2022/04/2022-04-21-devoxxfr-jeudi_02.jpg)


## 10:45 - 11:30 [Développer des applications observables pour la production](https://cfp.devoxx.fr/2022/talk/PFT-6511/Developper_des_applications_observables_pour_la_production)

[@PierreZ](https://twitter.com/PierreZ) de chez Clever Cloud nous fait un retour d'expérience sur comment il a changé sa manière de développer pour mieux gérer les astreintes, le tout via des applications observables

![](/2022/04/2022-04-21-devoxxfr-jeudi_03.jpg)

### Notes

#### Point sur l'Observabilité

- Software is eating the world!
- Le logiciel génère de la valeur quand il tourne en prod, pas quand on le dev
- Que fait le code quand il tourne ?
- Quand on dev, on a une représentation mentale de ce qu'on fait, mais elle peut etre fausse
- Tant qu'on ne mesure pas les algos, on ne peut pas savoir ce qui est le mieux
- Monitoring : est-ce que ça marche ? Oui/Non
- Architecture par service -> distribution des problèmes
- Observabilité : à quel point notre système fonctionne bien ?
- Papillon -> Ouragan : trouver le papillon = debugging

#### Point sur le Debugging

- Process qui permet de comprendre le systeme, doit pouvoir se faire en production
- Debugger c'est 3 étapes: se poser des questions, faire des observations, revenir au 1)
- En tant que dev, écrire du code qui permet de répondre aux questions de debug
- code "prod-ready" = code observable
- 3 méthodes pour debugger : *USE*, *RED*, *4 Signals*
  - Usage, Latency, Saturation, Errors
- Comment instrumenter:
  - 2 moyens: statique (logs, metrics, traces) & dynamique (ebpf, strace)
- Logs: si possible, mettre la mitigation

#### Opentelemetry

- merge entre OpenTracing & OpenCensus
- agnostique
- librairie qui commence a être vraiment pas mal
- plus orienté traces pour le moment
- Meilleur outil pour détecter les patterns : notre cerveau
- Flame graph: utile pour savoir comment s'enchainent les fonctions, et prennent en %CPU

### Conclusion

Première conférence très sympa. Même si je connais le sujet, c'est toujours bon de se faire rappeler les bases, et voir qu'on est en phase.

Même si la partie tracing est super intéressante, je pense que les logs et métriques resteront présentes encore un moment, surtout pour les applications qu'on ne maitrisent pas (mode blackbox).

#### Points à voir

- flamegraph

## 11:45 - 12:30 [Dans les coulisses du "Cloud"](https://cfp.devoxx.fr/2022/talk/RBN-3952/Dans_les_coulisses_du_%22Cloud%22)

[@AtaxyaNetwork](https://twitter.com/AtaxyaNetwork) nous présente le cloud, de quoi il est fait.

### Notes

- Partage sa passion, n'a pas connu le bug de l'an 2000
- Travail chez Aquaray (18 pers), infogérance LAMP. Propre DC, fibre, etc.
- Raconte son quotidien
  - L1 au L3, cablage, etc...
- Explication de ce qu'est un DC : tout et n'imp', tant que ça héberge de la data
- Comment on juge la qualité ? cf TIER
  - tolérance aux pannes
  - redondance
  - clim
- Comment on est certifié TIER4?
  - Systèmes de refroidissement redondants, topologie cold corridor
  - 2 voies électriques
  - Redondance réseau (DC propre + redondance sur 2 autres DC - TH2 et Interxion)
- Redondance réseau ? On enchaine sur Internet!
  - Meme si réseau maillé, y a qd meme une hiérarchie (tier1)
  - Qui gère les IP ? RIPE, en EU RIPE NCC
- BGP
  - 2x plus vieux que la speakeuse ;)
  - Pas de sécurité par défaut (historique, seulement géré par la confiance)
  - RFC 7454 donne les bonnes pratiques
  - Pour sécuriser BGP : RPKI
  - Ajout de clé privé pour prouver l'identité de l'AS
  - Le cas cloudfare: mauvaise annonce BGP

### Conclusion

Une jeune de 21 ans qui partage sa passion c'est toujours très agréable à voir ! Malgré un *gros* stress, on voit que Cécile maitrise son sujet et aime transmettre. Le sujet en soi était assez sympa, venant du réseau & système, j'ai quand même appris des trucs sur BGP !

## 13:00 - 13:15 [Simplifiez vos revues de code avec le rebase interactif](https://cfp.devoxx.fr/2022/talk/YOC-8839/Simplifiez_vos_revues_de_code_avec_le_rebase_interactif)

15 minutes qui sont passées trop vite, où [@sonia_seddiki](https://twitter.com/sonia_seddiki) nous explique les cas d'usages d'un `git rebase -i` 

![](/2022/04/2022-04-21-devoxxfr-jeudi_04.jpg)

### Notes

- Merge Request: toujours laborieux car on n'a pas le temps de vérifier que tout est bon
  - Niveau 0 "strict minimum": strict minimum (convention, propreté)
  - Niveau 1 "historique": trace des développements, contexte
- Le rebase interactif : rendre en 1 commit plusieurs commits
- plusieurs mots clés
  - pick (par défaut)
  - reword: renommer le message de commit
  - edit 
  - fixup: reorganiser l'historique

### Conclusion

Je reste sur ma faim, très bonne session, c'est dommage ! Bon en même temps ça tombe bien c'est l'heure de la pause repas ;)


## 13:30 - 14:15 [Protéger son organisation des attaques par le système de build](https://cfp.devoxx.fr/2022/talk/ZWH-7741/Proteger_son_organisation_des_attaques_par_le_systeme_de_build)

Dans un contexte plus dev/java @ljacomet nous montre comment bien protéger son système de build.

### Notes

- supply chain n'est plus une hypothèse
- exemples de corruption via supply chain : ccleaner, homebrew, solarwinds
- le hack se fait maintenant toujours par manière détournée, via les fournisseurs de soft
- voir "confusing dependencies at scale in 2020 (alex.birsan sur medium)
- Contexte sur l'ensemble des attaques possibles
- Gestion des PR: Attention, il peut meme y avoir des pb lors des jobs de CI sur l'infra lors des tests de PR
 - Seulement build sur l'infra interne après review humaine
- Pull Request Acceptance: CLAs, vérification de l'auteur de la PR, vérif "à la main", ou bien run sur environnement isolé
- Signer les commits!
- Améliorer la CI:
  - disposable containers, voir build cache

Sur les artfacts: voir l'intégrité  + checksum -> récupérer le binaire d'un côté, le checksum d'un autre !

### Conclusion

Sujet pas super rassurant, on voit qu'on peut être attaqué par tous les côtés !! Cela dit quelques bonnes pratiques à prendre que j'ai notée, à voir comment appliquer cela à nos infras.

### Points à voir

- *sigstore* pour la signature des binaires

## 14:30 - 15:15 [Cloud public, mais données privées !](https://cfp.devoxx.fr/2022/talk/YKJ-2069/Cloud_public,_mais_donnees_privees)

[@GillesSeghaier](https://twitter.com/GillesSeghaier), accompagné par le youtuber [@Micode](https://twitter.com/Micode) nous font un topo sur les aspects sécurité/confidentialité/disponibilité des données sur le cloud.

### Notes

- Accélération des "cloud outage"
- Quand 1 cloud provider tombe, si tout les outils sont liés, grosse dépendance, à faire attention
- outage: quand c'est "down" c'est la partie émergé de l'iceberg (gestion des données non dispo)
- leak des données: important aussi
- demo astrachain

### Conclusion

Désolé mais je n'ai pas super accroché à la conférence. D'une part parce que j'ai trouvé la 1ère moitié de la présentation trop longue, et d'autre part parce que j'avais déjà assisté à une session plus technique d'Astrachain au Devops DDay 2021, dont la vidéo est ici: https://www.youtube.com/watch?v=afp06HozxA4

## 15:30 - 16:15 [Eliminez la complexité de Kubernetes avec LENS !](https://cfp.devoxx.fr/2022/talk/RAX-2612/Eliminez_la_complexite_de_Kubernetes_avec_LENS)

L'équipe de @mirantis nous présente l'outil Lens, que je connais bien et utilise au quotidien.

### Notes

- IDE Kubernetes numero 1
- plus de 100K utilisateurs
- Utiliser k8s pour un dev c'est difficile -> Lens facilite tout cela
- 95% des utilisateurs trouvent que Lens facilite l'adoption et l'apprentissage
- Développé en Electron, facilite les plugins
- Tour du propriétaire de l'outil

### Conclusion

Mon ancien collègue Daniel Virassamy est sur scène, j'y suis un peu pour l'encourager, même si la salle est comble !

### Points à voir

- k0s pour cluster local
- extension debug pod!

## 16:45 - 17:30 [Montée de version sans interruption](https://cfp.devoxx.fr/2022/talk/DKU-3342/Montee_de_version_sans_interruption)

Nelson DIONISI de chez @mirakl nous fait un retour d'xp sur la gestion des mise à jour de leurs applications, avec un focus sur les upgrades de BDD.

![](/2022/04/2022-04-21-devoxxfr-jeudi_05.jpg)

### Notes
- Avant 2017, interruption de service lors d'upgrade : 1h = 125K€ de perte
  - 1 release tous les 3 mois, grosses montées de version
  - Trop de pbs -> migration sans interruption !! Use case de la BDD
- Règle primordiale: version N+1 de la BDD doit etre **retro-compatible** avec la version N de l'application
- En fait, un upgrade se fait en plusieurs étapes, pas d'un seul coup 
- Exemples dans les divers frameworks
- jamais de "select *"
- Sur les applis en fort trafic -> attention aux locks de la BDD
 - Identifier les opérations SQL lentes vs rapides pour optimiser les upgrades
 - ! Foreign key
- **mieux vaut casser la migration que l'application**

### Conclusion

Je pensais qu'on allait parler canary release, blue-green deployment, pas du tout ! Ce fut très enrichissant de voir comment est géré un upgrade côté soft, et pas *que* deploiement.

## 17:45 - 18:15 [Git, back to the future](https://cfp.devoxx.fr/2022/talk/LXE-2783/Git,_back_to_the_future) - [💻](https://links.zenika.com/link/antoine-ceol-devoxx-2022)|[⌨️](https://github.com/aceol/git-rewrite-history-devoxx)

Le quickie de midi trop court, je reprends un peu de GIT avec [@boombaprealm](https://twitter.com/boombaprealm) qui nous fait une belle démo de l'utilisation de git et ses plusieurs façons de ré-écrire l'histoire.

### Notes

- utilisation de "demo magic"
- git reflog : permet d'avoir les logs même après des append
- utilisation de rebase -i

### Conclusion

Sujet sympa, peut être malgré tout un peu long au final (oui je sais je trouvais trop court l'autre session, jamais content). Par contre j'ai bien aimé la façon de présenter + l'*astuce* `reflog`.

## 18:30 - 19:00 [REX: TDD avec TestContainers](https://cfp.devoxx.fr/2022/talk/OCB-6800/REX:_TDD_avec_TestContainers)

On a commencé les sessions avec Clever Cloud, on termine avec Clever Cloud ! [@juuduu](https://twitter.com/juuduu) nous explique l'utilisation des TestContainers.

### Notes

- Mise en place du système de facturation
  - zookeeper
  - pulsar
  - postgresql
- Testcontainers: lib java s'intégrant à JUnit et qui pilote du Docker.
  - voir sbt

### Conclusion

Même si le concept est intéressant, ce n'est malheureusement pas trop utile pour moi. Cela aura eu au moins le mérite de piquer ma curiosité !

## 20:00 - 20:50 [BOF Docker & Kubernetes](https://cfp.devoxx.fr/2022/talk/ITQ-7092/BOF_Docker_&_Kubernetes)

[@AdrienBlind](https://twitter.com/AdrienBlind), accompagné par [@ndeloof](https://twitter.com/ndeloof) souhaitent relancer les meetups autour de Docker & Kubernetes.

Le sujet du BOF déviera très rapidement sur les news Docker (licences Desktop, nouvelles features, etc)

### Notes
- Ce qui est dit au BOF reste au BOF ;)

## 21:00 - 21:50 [Il y a des cloud en Europe ?](https://cfp.devoxx.fr/2022/talk/EWW-6942/Il_y_a_des_cloud_en_Europe_%3F)

Animé par [@waxzce](https://twitter.com/waxzce) & [@LostInBrittany](https://twitter.com/LostInBrittany), une table ronde est faite autour du cloud en Europe. En gros, que faut-il en FR/EU pour que les entreprises n'aillent pas voir ailleurs ?
