---
title: "Kubernetes Nodes Logs avec Promtail"
date: 2022-06-30T12:00:00Z
thumbnail: /2022/06/2022-06-30-loki.png
categories:
  - Monitoring
tags:
  - IT
  - Kubernetes
  - Monitoring
description: "Utilisez les daemonsets Promtail pour récupérer les logs des nodes Kubernetes"
toc: true
---

[Promtail](https://grafana.com/docs/loki/latest/clients/promtail/) est un agent applicatif permettant de transmettre des logs vers une instance [Loki](https://grafana.com/docs/loki/latest/), outil de gestion de logs de la société Grafana Labs. L'outil fonctionne très bien en environnement Kubernetes, récupérant l'ensemble des logs des conteneurs, avec des notions d'auto-discovery et gestion de labels.

Il est très simple à installer en tant que daemonset dans un cluster, pour y récupérer les logs des conteneurs. Mais parfois on a besoin de récupérer également les logs des nodes du cluster, sur lesquels tournent ces conteneurs.

Ci-après une explication rapide pour la mise en place d'un tel setup.

![Promtail logs with Loki & Grafana](/2022/06/2022-06-30-promtail-logs.png)

## Installation simple

**Note**: Cet article utilise Promtail v2.5.0, la configuration présentée peut différer selon la version que vous utiliserez.

Par défaut, l'installation de Promtail se fait avec un chart Helm, qui fonctionne "out-of-the-box". On considère que Loki et Grafana sont déjà déployés dans le cluster.

```shell
$ helm repo add grafana https://grafana.github.io/helm-charts
$ helm repo update
$ helm --install --namespace observability --create-namespace promtail grafana/promtail
```

On peut voir maintenant que les agents Promtail tournent sur les nodes, et que les logs arrivent dans Loki. Grafana permet l'exploration des logs, tout est bon.

```shell
$ kubectl get pods -l app.kubernetes.io/name=promtail
NAME READY STATUS RESTARTS AGE
promtail-47jwn 1/1 Running 0 13m
promtail-mwkcb 1/1 Running 0 14m
```

![Promtail basic logs](/202/06/2022-06-30-promtail-basic-logs.png)

## Paramétrage

Pour l'instant, on a un setup simple, mais fonctionnel.

Cependant, pour les besoins exprimés cela ne suffit pas, car on doit transmettre les logs du répertoire `/var/log/` des nodes vers Loki, ici plus spécifiquement le fichier `/var/log/syslog`. Pourquoi ne pas utiliser ces daemonsets pour cela ? On va configurer Promtail en ce sens.

### Montage du dossier hôte

Tout d'abord, on doit monter le dossier de l'hôte `/var/log` sur le pod. On montera le dossier dans `/var/log/host` pour éviter tout problème d'écrasement / nommage.

On spécifie cela dans les paramètres du chart Helm, dans un fichier ` custom-values.yaml` :

```yaml
# Mount folder /var/log from node
extraVolumes:
  - name: node-logs
    hostPath:
      path: /var/log

extraVolumeMounts:
  - name: node-logs
    mountPath: /var/log/host
    readOnly: true
```

On upgrade le chart helm, et le dossier `/var/log/host` est bien là, parfait !

```shell
$ helm upgrade --install --namespace observability promtail grafana/promtail -f custom-values.yaml
$ kubectl exec -it promtail-mwkcb --mount | grep /var/log/host
/dev/sda1 on /var/log/host type ext4 (ro,relatime,data=ordered)
```

Avant d'aller plus loin, voyons si l'on peut lire les fichiers à l'intérieur :

```shell
$ kubectl exec -it promtail-mwkcb --tail /var/log/host/syslog
tail: cannot open ‘/var/log/host/syslog’ for reading: Permission denied
command terminated with exit code 1
```

Mmmm, il y a quelque chose d'incorrect ici, voyons les permissions des fichiers :

```shell
$ kubectl exec -it promtail-mwkcb — ls -l /var/log/host/syslog
-rw-r---- 1 102 adm 72327736 Jun 29 20:01 /var/log/host/syslog
```
Le fichier appartient à l'utilisateur avec l'ID 102 et groupe "adm". Avec `getent` on a un peu plus d'infos sur ce groupe :

```shell
$ kubectl exec -it promtail-mwkcb -- getent group adm
adm:x:4:
```

Donc, si l'on veut que promtail puisse lire ce fichier, il faut ajouter des contextes de sécurité avec l'option `fsGroup`, que l'on définit dans les custom values du chart Helm :

```yaml
# Set fsGroup to allow syslog file reading
podSecurityContext:
  fsGroup: 4
```

On upgrade le chart Helm, et on vérifie l'accès au fichier :

```shell
$ helm upgrade promtail grafana/promtail -f custom-values.yaml
$ kubectl exec -it promtail-2dbh9 -- tail /var/log/host/syslog
Jun 29 20:16:28 worker-pool-node-d5353e kubelet[1596]: I0629 20:16:28.374480 1596 clientconn.go:897] blockingPicker: the picked transport is not ready, loop back to repick
Jun 29 20:16:28 worker-pool-node-d5353e docker[1888]: I0629 20:16:28.375267 1 utils.go:81] GRPC call: /csi.v1.Node/NodeGetVolumeStats
```

Cela semble mieux ! Maintenant on peut lire les fichiers du node depuis notre pod Promtail.

## Configuration Promtail

On va ensuit configurer Promtail pour lire (scrape) ce nouveau fichier, avec un simple "File Target Discovery" (voir [ici](https://grafana.com/docs/loki/latest/clients/promtail/scraping/#file-target-discovery)). On y rajoute quelques labels pour les retrouver facilement dans Loki plus tard. Toujours dans les custom values :

```yaml
# Scrape config to read syslog file from node
config:
  snippets:
    extraScrapeConfigs: |
      # Add an additional scrape config for syslog
      - job_name: node-syslog
        static_configs:
        - targets:
          - localhost
          labels:
            job: node/syslog
            __path__: /var/log/host/syslog
```

On upgrade une fois de plus le chart, et on peut maintenant voir les logs syslog du node, en plus des logs des conteneurs, être transmis dans Loki, nickel !

![Nodes logs with Promtail](/2022/06/2022-06-30-promtail-nodes-logs.png)

## Ajout de labels dynamiques

Les logs sont là, mais avoir des labels supplémentaires ne serait pas de refus, comme le nom du node.

On va alors utiliser l'option `-config.expand-env` qui récupère les variables d'environnement, et on va les rajouter dans la configuration de Promtail :

```yaml
# Allow environment variables usage
extraArgs:
  - -config.expand-env=true

# Scrape config to read syslog file from node
config:
  snippets:
    extraScrapeConfigs: |
      # Add an additional scrape config for syslog
      - job_name: node-syslog
        static_configs:
        - targets:
          - localhost
          labels:
            job: node/syslog
            __path__: /var/log/host/syslog
            node_name: '${HOSTNAME}'
```

On upgrade une dernière fois le chart, on vérifie les logs dans Grafana... Et voilà ! On a bien les logs des nodes, avec les labels qui vont bien !

![Labelled nodes logs with Promtail](/2022/06/2022-06-30-promtail-nodes-logs-labels.png)

## Récapitulatif

Pour résumer, afin d'avoir les logs des nodes dans Loki en utilisant Promtail, il "suffit" de :

1. Monter le dossier `/var/log` du node dans le pod Promtail
2. Utiliser le bon `fsGroup` pour pouvoir lire les fichiers
3. Ajouter une configuration de scraping `static_configs`
4. Utiliser les variabels d'environnement pour ajouter des labels

On retrouvera le fichier `custom-values.yaml` pour déployer le chart Helm dans mon [dépôt GitLab](https://gitlab.com/rverchere/promtail-host-logs).

On peut maintenant fouiner dans les logs, vérifier ce qu'il ne va pas sur les nodes, et plus encore... Bonne recherche !
