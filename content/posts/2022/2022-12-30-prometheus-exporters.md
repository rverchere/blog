---
title: "Quelques Exporters Prometheus pratiques"
date: 2022-12-30T00:00:00Z
thumbnail: /2022/12/2022-12-30-prom-exporters.png
categories:
  - Monitoring
tags:
  - Monitoring
  - IT
  - Kubernetes
description: "Quelques exporters prometheus pratiques pour administrer un cluster Kubernetes"
toc: true
---

Suite à un mini thread [Twitter](https://twitter.com/rverchere/status/1606237656865771520), je mets par écrit ici quelques lignes sur les exporters Prometheus qui peuvent s'avérer pratiques pour administrer un cluster Kubernetes.

## X509 Certificate Exporter

Proposé par la société [Enix](https://enix.io/fr/), l'exporter [x509-certificate-exporter](https://github.com/enix/x509-certificate-exporter) permet de suivre les certificats x509 de vos applications, et être notifié sur l'expiration de ceux-ci.

J'utilise en général [cert-manager](https://cert-manager.io/) et [Let's Encrypt](https://letsencrypt.org/), qui permet d'automatiser la création et renouvellement de certificats, surtout au niveau ingress et HTTPS, mais cela n'empêche pas d'avoir des alertes sur le renouvellement des certificats :
- Le challenge Let's Encrypt qui échoue pour diverses raisons (principalement des erreurs réseaux).
- Un secret Kubernetes qui contient un ancien certificat qui n'est pas nettoyé.

Un dashboard est même fourni avec, que vous pourrez modifier selon vos envies :

![x509 dashboard](https://raw.githubusercontent.com/enix/x509-certificate-exporter/main/docs/grafana-dashboard.jpg)

## Helm Exporter

J'utilise beaucoup de charts Helm pour déployer les applications sur Kubernetes, surtout les briques d'infrastructure (prometheus, nginx, cert-manager, velero, etc.).
G
Je souhaitais alors pouvoir suivre les versions de charts Helm sur l'ensemble des clusters administrés, et je suis tombé sur 2 outils:

1. [Nova](https://nova.docs.fairwinds.com/) de la société Fairwinds, un outil en CLI qui permet de lister les charts dépréciés. L'outil est pratique, mais difficile d'avoir une visualisation globale, ainsi qu'un tableau de bord

2. L'exporter [helm-exporter](https://github.com/sstarcher/helm-exporter) qui va générer des métriques Prometheus pour les charts installés au sein du cluster, avec les versions également dépréciées.

Je me suis arrêté sur ce deuxième choix, car il est ainsi facile d'avoir un dashboard dans Grafana.

La configuration est un tout petit peu "pénible", dans le sens où il faut définir la source de chaque chart (le tout est expliqué sur le dépôt GitHub).

```yaml
config:
  helmRegistries:
    override:
      - registry:
          url: "https://charts.jetstack.io"
        charts:
          - "cert-manager"
        allowAllReleases: true
      - registry:
          url: "https://charts.gitlab.io"
        charts:
          - "gitlab-runner"
        allowAllReleases: true
      - registry:
          url: "https://shanestarcher.com/helm-charts"
        charts:
          - "helm-exporter"
        allowAllReleases: true
      - registry:
          url: "https://kubernetes.github.io/ingress-nginx"
        charts:
          - "ingress-nginx"
        allowAllReleases: true
[...]
```

Ainsi, on peut voir que mes clusters ont bien le chart précédemment installé à jour ;)

![helm exporter](/2022/12/2022-12-30-prom-exporters-helm.jpg)

## Blackbox Exporter

On revient aux fondamentaux, le [blackbox_exporter](https://github.com/prometheus/blackbox_exporter) reste un exporter très utile lorsqu'on doit monitorer des sites ou équipements distants, ne répondant qu'en HTTP(S), DNS, TCP.

Je l'utilise surtout pour avoir les temps de réponse des sites déployés, depuis un site tiers.

![website metrics](/2022/12/2022-12-30-prom-exporters-blackbox.png)

## Harbor Exporter

Un petit dernier, quand on est en environnement conteneurs et qu'on gère une registry (Harbor notamment), on peut suivre son état avec celui-ci. Il existe l'exporter [harbor_exporter](https://github.com/c4po/harbor_exporter).

Je l'utilise assez peu, mais cela me permet de voir quels projets consomment le plus de place quand il faut y faire du ménage (il est si facile de créer des images Docker...).

![harbor exporter](https://raw.githubusercontent.com/c4po/harbor_exporter/master/grafana/screenshot.png)

## Prometheus MS Teams

Ce n'est pas un exporter, mais je note quand même l'outil [prometheus-msteams](https://github.com/prometheus-msteams/prometheus-msteams), qui est un serveur web léger en Go qui permet de récupérer des alertes générées via webhook pour les transmettre à un canal MS Teams.

![promteams](https://raw.githubusercontent.com/prometheus-msteams/prometheus-msteams/master/docs/promteams.png)

Testé chez moi avec succès !

## Cabourotte & Appclacks

Si cela nous vous suffit pas, allez voir aussi ce que fait [@mcorbin](https://www.mcorbin.fr/), avec [Appclacks](https://www.doc.appclacks.com/) et sa sonde health-check [cabourotte](https://www.cabourotte.appclacks.com/). Projet prometteur !

Les *probes* se pilotent via API au travers d'une CLI, et il existe même un provider [Terraform](https://github.com/appclacks/terraform-provider-appclacks). On peut soit utiliser les sondes fournies, soit ses propres sondes avec cabourotte, qui ira récupérer sa config auprès du serveur central.


## D'autres ressources

Dernier petit lien, si vous devez administrer un serveur Mastodon (rien à voir avec Kubernetes, certes), allez voir [ici](https://medium.com/@dma42/observing-hachyderms-24e0f35529a5).
