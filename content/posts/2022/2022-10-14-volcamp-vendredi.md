---
title: "Notes Volcamp 2022 - Vendredi"
date: 2022-10-14T12:00:00Z
thumbnail: /2022/10/2022-10-13-volcamp.png
categories:
  - Conférence
tags:
  - Volcamp
  - IT
  - Développement
  - Kubernetes
description: Retour sur la conférence Volcamp 2022, 1er jour.
toc: true
---

## Volcamp, deuxième journée

Après une première journée bien remplie, on rattaque !

Pression supplémentaire, c'est aujourd'hui que je fais une présentation. J'en parlerai dans le paragraphe dédié.
Du coup, les notes sont moins fournies, étant concentré sur ma session.

![Jour 2](/2022/10/2020-10-14-volcamp-jour-2.jpg)

## Keynote

[Ludovic Cinquin](https://twitter.com/Lcinquin), CEO d'Octo, nous prédit l'usage des technos dans les 10 prochaines années, équilibre entre adoption et disruption.

Il avait déroulé cette conf à DevoxxFR quelques mois plus tôt, à laquelle j'avais assisté. Je connaissais donc le sujet, même si mis à jour avec les (nombreux) événéments qui se sont déroulés entre temps.

Pour en savoir plus, il suffit de (re)lire [mes précédentes notes](/posts/2022/2022-04-22-devoxxfr-vendredi/#0900---0920-futurospective-digitale--le-futur-est-il-encore-ce-quil-%C3%A9tait-httpscfpdevoxxfr2022talkbmv-7373futurospective_digitale__le_futur_est-il_encore_ce_que28099il_etait_3f) ;)

![Keynote](/2022/10/2022-10-14-keynote.jpg)
*Crédits Image : [Régis Allègre](https://www.linkedin.com/posts/regisallegre_coding-sustainability-volcamp2022-activity-6986749787228241920--y9S)*

### Commentaires

> Approche toujours pertinente, et rappeler le concept de right tech est plus que nécessaire !

## Comment casser sa prod avec des logs ? (En toute efficacité)

Avec plein d'autodérision, le binôme [Clément Grégoire](https://twitter.com/lectem), Cédric Charière--Fiedler nous montrent tous les anti patterns de la gestion des logs.

On y parle de mauvaise rotation, logs trop verbeux, inutiles, et comment l'éviter.

Encore plus loin niveau code, les mauvais usages de code custom qui surcharge les loggers, et qui font au final n'importe quoi.

![Gestion des Logs](/2022/10/2022-10-14-logs.jpg)

### Commentaires

> A voir si vous avez déjà eu un problème de gestion de logs, vous vous y retrouverez à coût sur.
>
> Pour avoir déployé des infrastructures de logs, le sujet me parle beaucoup, et le sujet est traité avec "légèreté", je recommande !

## Comment nous avons géré le syndrome de fatigue des alarmes en réduisant le bruit généré par nos alertes.

Doctolib, représenté par [Gauthier François](https://twitter.com/gnublin), nous fait un retour d'expérience sur le syndrome de l'alarme fatigue, et comment son équipe a réduit drastiquement ces alertes.

Le constat : lors de la migration de leur application vers un environnement conteneurisé, les métriques & alertes n'ont pas été revue. Ainsi, les équipes SRE et astreinte ont vu le volume d'alertes exploser, en lien avec le nombre de composants de leurs clusters.

Il a fallu donc faire du ménage... mais comment ?

Après avoir donné leur définition du bruit (alerte qui se résoud seule au bout de 2min), du budget qu'il souhait mettre (X alertes par trimestre), Gauthier nous fait part de leur méthodologie pour nettoyer tout cela.

De façon simple pragmatique (mot qui définit très bien Gauthier), sont revues la pertinence des alertes, leur duplicité (N alertes pour 1 même problème), les seuils à gérer (les warning ne servent jamais, on repousse toujours jusqu'à une alerte critique où l'on intervient enfin).

On est aussi dans l'amélioration continue, avec des itérations courtes sur les changements, modifications de seuils, etc.

Ainsi le bruit a été réduit drastiquement, jusqu'à avoir un effet pervers : le silence... Inhabituel, jusqu'en oublié l'astreinte !

![Gauthier François](/2022/10/2022-10-14-alerte-fatigue.jpg)

### Commentaires

> Très beau REX simple, sans prétention, l'intervention de Gauthier était claire. Pour une première conf, ce fut une belle surprise !

## Velero - Sauvegardez et Restaurez (proprement) vos applications kubernetes

A mon tour de monter sur scène !

Première fois que je présente ce sujet, parti d'un constat qu'il manquait de ressources en français sur la configuration de l'outil Velero pour gérer mes backups en environnement conteneurisé. Et le "peu" de ressources type blogpost n'allaient pas suffisamment loin.

J'ai donc essayé de compléter cela avec mon expérience sur le sujet, en y apportant ma compréhension, mes cas d'usages, mes erreurs... Difficile par contre de présenter un produit qu'on connait certes, mais en tant que "simple" utilisateur, n'ayant pas traité tous les uses-cases possibles.

La conf s'est déroulée sans accrocs, dans les temps, avec un public plutôt bienveillant, même si j'ai remarqué beaucoup de baillements dans le public ! Il faut dire que la session était juste après le buffet du midi, avec aligot au menu !!

J'ai déjà remarqué quelques coquilles, améliorations pour la prochaine fois.

Si vous lisez ce blogpost et avait assisté à ma conf, je suis preneur de retour ! Les slides sont également disponibles  [ici](https://presentations.verchere.fr/Backup_Velero/). Si j'ai dit des bétises ou si vous avez des compléments d'information, n'hésitez pas à m'en faire part !

![Rémi Verchère](/2022/10/2022-10-14-velero.jpg)

## GPS précis, ISOBUS, coupure de sections, drones, robots, analyses... la place de la tech dans l'agriculture d'aujourd'hui

Avec un teasing sur Twitter aussi impressionnant que la longueur du titre de son talk, [Mathieu Passenaud](https://twitter.com/mathieupassenau) nous fait un tour des technologies sur les engins agricoles.

Avec des supports videos, exemples, recherches auprès des constructeurs de machines agricoles, on traite des diverses technos disponibles : la gestion de la pression des pneus pour gérer le tassage, la répartition du poids avec les engrais, la détection des produits avec capteurs (tomates, grains, etc.), le déplacement des machines à l'aide du GPS, et GPS RTK.

Même si la techno embarqué n'est pas compliquée, son usage reste impressionnant ! 

En plus du sujet technique, une vrai sensibilisation autour du travail du monde agricole est faite. Respectez les agriculteurs et ce métier noble de travail de la terre et des animaux !

![Mathieu Passenaud](/2022/10/2022-10-14-agriculture.jpg)

### Commentaire

> J'attendais ce talk avec impatience, le sujet avait très bien été vendu sur les réseaux sociaux... Et connaissant un tout petit peu le speakers, j'étais sur de ne pas être déçu. Et cela a été confirmé par une salle pleine !
>
> C'était mon dernier talk avant de rentrer, on peut dire que je termine en beauté. Bravo !

## Les "à côté" de la conf.

En plus d'être présent pour suivre les confs, j'ai été sur scène, mais également sur le stand de ma société, qui était partenaire de l'événement.

Fun fact, on m'a proposé à participer à l'orga côté partenaire après avoir été sélectionné, j'ai donc cumulé plusieurs casquettes.

En vrai, il y avait suffisamment de monde pour tenir le stand, ce qui m'a permis de voir suffisament de présentations et faire du "networking".

Cela m'a permis aussi de rencontrer des collègues, la boite où je travaille actuellement étant assèz grosse.

![Stand Accenture](/2022/10/2022-10-14-stand.jpg)

L'avantage des confs "à taille humaine" comme Volcamp, c'est que la proximité speakers/participants est la, et tout le monde reste disponible, sans stress. Je pense que le fait d'avoir participé au camping de speakers l'été dernier a aussi pas mal jouer dans ma relation que je peux avoir avec certains DevRels & speakers plus expérimentés, c'est très sympa.

Aucun couac apercu sur l'orga, franchement bravo. Et on sent les locaux bien dans leurs bottes, c'est appréciable. Avec un point bonus sur le buffet local, on s'est régalé !

Si vous souhaitez assister à une conférence pas trop grosse, sans prise de tête mais avec quand même des sujets techs sérieux, c'est le bon endroit !

Je vous dit très probablement à l'année prochaine !
