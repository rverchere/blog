---
title: "Notes DevOps D-Day 2022"
date: 2022-12-01T23:59:59Z
thumbnail: /2022/12/2022-12-01-banniere-devops-dday.png
categories:
  - Conférence
tags:
  - DevOps D-Day
  - IT
  - Développement
  - Kubernetes
description: Retour sur la conférence DevOps D-Day 2022.
toc: true
---

## DevOps D-Day

Comme presque chaque année depuis 2015 (pas merci la COVID), la conférence DevOps D-Day se tient à Marseille, dans le fameux stade Orange Vélodrome.

C'est un événement à ne pas manquer lorsqu'on est en région !

Pour moi, cette conférence est assez particulière, car c'est un peu grâce à cet événement que j'ai découvert et me suis lancé dans l'aventure "DevOps", "Cloud Native", "Conteneurs" and co. J'ai expliqué tout cela d'ailleurs sur un [thread Twitter](https://twitter.com/rverchere/status/1594631932926889984).

De plus, cette année, je suis speaker, en présentant un talk sur [Velero](https://presentations.verchere.fr/Backup_Velero_DevOps_Dday_2022/), outil de sauvegarde en environnement Kubernetes.

Enfin, vous verrez que mes notes sont assez concises. Sortant d'une opération au visage (celles & ceux qui m'ont croisé l'ont remarqué), j'ai du me préserver pour être en forme pour ma session, avec quelques pauses repos. Je n'ai d'ailleurs pas "live-tweeté" pour la même raison. Mais pas d'inquiétude, je vais bien ;)

Passons maintenant au debrief !

![Prêt à rentrer dans l'arène !](/2022/12/2022-12-01-stade.jpg)

## La conférence, ambiance, environnement, etc.

Le stade Orange Vélodrome fait toujours son petit effet quand on y est ! Les nouveaux participants se prennent en photo devant le stade, et on croise beaucoup d'anciens collègues toujours aussi ravis d'être là !

Cette année, les conférences sont réparties sur 5 salles, les sessions étant organisées par catégories de sujets : cela permet aux participants de mieux suivre des "tracks" s'ils le veulent.

À l'étage principal, on y retrouve les partenaires avec les stands, ainsi que le coin repas.

On m'a fait la remarque qu'il y avait moins de personnes que l'an dernier. Au contraire, j'ai eu l'information que le public était plus nombreux (à confirmer par les organisateurs), mais le fait d'avoir 1 salle en plus et plus d'espace au niveau des stands donnait cette impression.

A titre personnel, je n'aime pas être les uns sur les autres, donc si l'événement permet à plus de participants de venir ET de pouvoir circuler librement, je dis banco !

Place maintenant aux conférences. J'ai essayé de suivre des sujets que je connais ou ne maitrise pas, afin de découvrir toujours de nouvelles choses.

## UnFIX, l’anti framework d’agilité à l’échelle ?

[Clément Rochas](https://twitter.com/crochas) et [Thomas Clavier](https://twitter.com/thomasclavier) nous présente le framework "unFIX", qui permet de modéliser SON entreprise, avec plein de carrés, ronds, rectangles de toutes les couleurs !

Pour les agilistes, cela permet d'avoir une "librairie" commune pour s'organiser et bien communiquer, en utilisant son propre vocabulaire.

Après une présentation de chaque composant / team, le duo enchaine les questions / réponses sur pourquoi unFIX, ce que cela peut améliorer, comment le mettre en œuvre.

Quelques points intéressants :
- Les managers se retrouvent en équipe, et doivent donc fonctionner en équipe
- La notion de forum permet d'avoir des communautés inter-équipes, mais où l'on ne doit pas passer tout son temps
- On sait que tout n'est pas figé, et donc il est possible de faire des équipes temporaires, avec certaines règles (fréquence, stabilité, etc.)

UnFIX est vraiment récent, et n'a été "découvert" qu'en début d'année 2022.

Un [blog post](https://aqoba.fr/posts/20221129-%C3%A0-la-d%C3%A9couverte-dunfix/) d'Aqoba est disponible pour celles & ceux qui veulent en savoir plus.


![UnFIX](/2022/12/2022-12-01-unfix.jpg)

### Commentaires

> Très bon démarrage de conf, j'ai noté quelques bouquins à lire (encore), la salle était pleine, le sujet intéressant. Ce n'est pas mon rôle de définir ce genre d'organisation, mais c'est toujours bien de savoir que cela existe le jour où une réorg s'annonce ;)

## Tips pour combattre le syndrome de l'imposteur

Suite à un [aléa du direct](https://twitter.com/aurelievache/status/1598432322365300738?s=20&t=d6REBmUcmbh_miTze2I_aQ) ;) j'ai été aux premières loges pour la présentation d'[Aurélie Vache](https://twitter.com/aurelievache) sur le syndrome de l'imposteur, et comment transformer cette "faiblesse" en force.

En salle plénière, Aurélie nous explique ce qu'est ce syndrome, comment le combattre, et même comment en tirer parti.

Le mot "humilité" résume bien les personnes atteintes de ce syndrome.

Beaucoup de questions et remarques en fin de talk, car c'est un sujet universel.

### Commentaires

> Une partie du sujet avait été abordé à Volcamp, on ne peut pas être insensible à ces propos. Pour être franc, j'avais prévu d'assister à une autre conf plus tech, heureusement, il devrait y avoir le replay ;)

## Rendez l'agilité aux développeur(se)s !

Une autre conf orienté orga & équipe, avec [Fanny Klauk](https://twitter.com/klf37) qui nous donne les "clés" pour qu'une équipe de dev travaille dans de bonnes conditions !

La présentation raconte l'histoire de Lily, jeune développeuse dans une équipe de dev, qui participe à des sprints. On y retrouve pas mal de douleurs comme l'outil de ticketing pas adapté, les KPIs perso versus équipe, les feedbacks clients qui arrivent trop tard, etc.

A chaque étape, une solution est proposée pour que l'équipe, et notamment Lily, se sente bien dans son job et avance sereinement.

![Agilité Devs](/2022/12/2022-12-01-agilite-dev.jpg)

### Commentaires

> Un autre sujet éloigné de mon quotidien, mais toujours intéressant ! Et ayant croisé Fanny sur d'autres événements & les réseaux sociaux, j'étais curieux de la voir sur scène. Sujet maitrisé devant une salle pleine, avec un ton juste assorti de quelques blagues et légères piques, j'adhère !

## Filtrage des flux réseaux issus des clusters Kubernetes : pourquoi, comment ?

J'avais dit que j'essayais d'aller voir des sujets que je ne connaissais pas, là sur le coup, c'est raté ;)

BlueTrusty, partenaire Calico, nous présente les différentes façons de gérer les flux réseaux sortants d'un cluster Kubernetes, avec plusieurs possibilités d'implémentation : Egress Network Policy, firewall classique, etc.

Plusieurs stratégies sont présentées ci-après.

### Stratégie 1 : Network Policies

Utilisation des Network Policies classiques de Kubernetes.

Tant que pas de règles sur un pod, c'est du allow all.

Avec la CNI Calico, il existe des "GlobalNetworkPolicy" pour simplifier la création de règles, car par défaut les règles sont par namespace ce qui peut être vite complexe à gérer. Calico, dans sa version entreprise , permet également de mettre un ordre sur les règles de firewall pour que les administrateurs réseaux s'y retrouvent.

### Stratégie 2 : Node Selector

L'idée est de créer des règles de FW sur les équipements externes au cluster, puis de placer les pods sur un nœud spécifique, qui sera lui filtré.

C'est une solution peu commode, car on perd en fonctionnalité sur le cluster, obligeant à placer les workloads.

### Stratégie 3 : Label Selector et Bridge Mode

Chaque pod a une IP "publique", et donc on peut filtrer soit par IP de pod, soit par subnet, etc.

Comme la stratégie 2, cela oblige à gérer un pool d'IP "externe".

### Stratégie 4 : Egress Gateway

Le dernier point est d'utiliser un pod dédié, part lequel sort tous les flux réseaux avec du snat.

### Conclusion

Au final, il n'y a pas de meilleure stratégie, cela dépend du contexte, comme d'habitude !

Les slides sont disponibles [ici](https://dl.bluetrusty.eu/SREYTAN_DevOpsD-Day_01Dec22.pdf) !


![Filtrage Réseau](/2022/12/2022-12-01-network-k8s.jpg)

### Commentaires

> Le discours est orienté équipes réseaux, qui ont l'habitude de gérer des interfaces firewall, et peu habituées à écrire du yaml pour définir des règles, dommage, car je connais les deux, et ça marche très bien ensemble ;)

> Vue d'ensemble assez sympa, même si j'aurais aimé aller plus loin dans la technique, le côté Network Policies est "basique", mais je ne doute pas que [Stephane Reytan](https://twitter.com/bluetrusty_fr) saurait aller plus loin dans un deep dive.

## Gérer les drifts des ressources Terraform grâce à la méthode GitOps

Un autre sujet qui me "passionne" aussi, Terraform & GitOps ensemble ça ne peut être qu'intéressant !

[Katia Himeur Talhi](https://twitter.com/katia_tal) nous explique ce qu'est un drift, comment le détecter, et le gérer !

Plusieurs solutions : ne rien faire (sic), avoir un workflow dédié, ou bien avoir quelque chose qui permet l'auto-remédiation.

Dans ce contexte, elle nous présente le couple Terraform + Flux2, avec le plugin "Weave GitOps Terraform Controller" en alpha release.

Cet outil permet simplement de gérer son code TF en mode GitOps avec Flux, et donc de bénéficier de flux avec la détection en read only, la validation manuelle, la gestion de dépendances, etc.

Enfin, elle termine par une démo simple qui met bien en avant les avantages de la solution.

Bonus, les slides sont disponibles [ici](https://fr.slideshare.net/KatiaHimeur/gestion-des-drifts-terraform-avec-la-mthode-gitops)

![Drit GitOps - Crédits @katia_tal](/2022/12/2022-12-01-drift-gitops.jpg)

### Commentaires

>J'ai découvert Katia lors d'un talk à l'OSXP 2021, sur un sujet Flux2, j'ai été ravi de la revoir sur Marseille avec un sujet équivalent, en allant plus loin avec Terraform. TF Controller est à surveiller, je pense que cela pourra faire un bon sujet de workshop interne.

## Kubernetes : la solution à tous vos problèmes IT ? (Spoiler alert : non)

[Thomas Rannou](https://twitter.com/thomas_rannou), après avoir fait un tour de ce qu'est Kubernetes, nous présente d'autres solutions qui peuvent répondre à des besoins, sans sortir forcément une artillerie lourde.

L'ensemble des solutions présentées sont liées au cloud provider MS Azure.

### Commentaires

> Désolé, mais je n'ai pas pris de notes, car cela était juste avant ma conf, et je commençais à avoir quelques problèmes oculaires ! Donc niveau concentration, j'étais un peu ailleurs. Promis, je regarderai le replay ;)

## Velero : sauvegardez et restaurez (proprement) vos applications Kubernetes

A mon tour de présenter Velero ! Rien à dire de particulier, je connais mon sujet (enfin, je crois !) ;)

Les slides sont [ici](https://presentations.verchere.fr/Backup_Velero_DevOps_Dday_2022).

![Velero - Crédits Lucas](/2022/12/2022-12-01-velero.jpg)

### Commentaires

> J'étais plus stressé par mon état physique que par ma présentation, et du coup, j'étais plutôt détendu, et le public assez intéressé. De bons retours post-conf, ça fait toujours plaisir !

## Révolution eBPF : un noyau Linux dynamique

On finit la journée par un sujet qui m'intéresse particulièrement, eBPF !

[Raphaël Pinson](https://twitter.com/raphink) de la société Isovalent nous présente eBPF, qui l'utilise, comment, les cas d'usages, sa "limite" avec le kernel Linux. 

Au-delà du cas d'usage le plus commun de l'observabilité dans kubernetes, il nous présente d'autres perspectives d'utilisation niveau réseau et sécurité notamment, agrémenté d'exemples et interactions du noyau.

![eBPF](/2022/12/2022-12-01-ebpf.jpg)

### Commentaires

> Malgré le créneau de fin de journée, qui n'est pas évident car beaucoup de participants sont déjà partis, beaucoup de monde présent pour cette session super intéressante ! Et en plus on a eu droit à des stickers :P

> Même si je n'ai pas tout suivi (je venais de terminer ma conf), le sujet est passionnant, on sent que c'est un sujet qui va prendre beaucoup de place ces prochaines années. A suivre...

## Pour finir

Encore une belle édition, même si je n'étais pas à 100% de mes capacités. J'espère y revenir l'an prochain, avec toujours autant d'amis, anciens et nouveaux collègues, partenaires à croiser sur les stands !

Dommage de ne pas avoir pu assister à certains talks, il y en a que j'aurai beaucoup aimé voir, mais je n'ai pas encore le don d'ubiquité...

Rendez-vous en 2023 !